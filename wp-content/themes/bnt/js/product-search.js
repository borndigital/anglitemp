(function($) {
  $(document).ready(function() {
    // when we submit the product search, look for products
    $('.product-search').on('submit', function(e) {
      e.preventDefault();
      var search = $('#search').val();
      search_products(search, handle_search_products);
      return false;
    });

    // when we stop typing look for products
    $('.product-search input').smartkeyup(function() {
      var search = $(this).val();
      search_products(search, handle_search_products);
    }, 500);
  });

  function search_products(s, callback) {
    $.ajax({
      url : '/api/products',
      data: {
        search: s
      },
      success: callback
    });
  }

  function handle_search_products(response) {
    // update the products markup
    $('.products').html(response.html);

    // apply our monochrome effect to the images
    window.monochrome_products();
  }
})(jQuery);
