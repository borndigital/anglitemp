(function($) {

  // when we submit the product search, look for products
  $('.price-list-search').on('submit', function(e) {
    e.preventDefault();
    var search = $('#search').val();
    search_price_list(search, handle_search_price_list);
    return false;
  });

  // when we stop typing look for products
  $('.price-list-search input').smartkeyup(function() {
    var search = $(this).val();
    search_price_list(search, handle_search_price_list);
  }, 500);

  function handle_search_price_list(response) {
    // update the price list markup
    $('.price-list').html(response.html);
  }

  function search_price_list(s, callback) {
    $.ajax({
      url : '/api/price_list',
      data: {
        search: s
      },
      success: callback
    });
  }

})(jQuery);
