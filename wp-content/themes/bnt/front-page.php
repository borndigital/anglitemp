<?php get_header(); ?>

<div class="container">

  <?php get_template_part('parts/product/search'); ?>
  
  <?php get_template_part('parts/home/products'); ?>

  <?php get_template_part('parts/home/suppliers'); ?>

  <?php get_template_part('parts/home/distributors'); ?>

  <?php get_template_part('parts/home/offers'); ?>  

</div>

<?php get_footer(); ?>