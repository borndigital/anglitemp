<?php get_header(); ?>

<div class="container">

  <?php get_template_part('parts/product/search'); ?>

  <?php get_template_part('parts/product/info'); ?>

  <?php get_template_part('parts/contact/branch-select-compact'); ?>

  <?php get_template_part('parts/product/supplier-testimonial'); ?>

  <?php get_template_part('parts/product/testimonial'); ?>

</div>

<?php get_footer(); ?>