<?php 
/*
Template Name: Price list
*/
get_header(); ?>

<div class="container">
  
  <?php get_template_part('parts/prices/search'); ?>
  
  <?php get_template_part('parts/prices/list'); ?>

</div>

<?php get_footer(); ?>