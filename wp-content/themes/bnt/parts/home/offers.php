<section>
  <div class="row">

    <div class="col-sm-4">
      <div class="latest-offers home-block-small">
        
        <div class="row">
          <div class="col-xs-7 col-sm-16">
            &nbsp;<br>
            <span class="big">News</span><br>
            <div class="sep"></div>
            <br />
          </div>
          <div class="col-xs-9 col-sm-16">
            <div class="controls">
              <span class="news-prev"><i class="fa fa-chevron-left"></i></span>
              <span class="news-next"><i class="fa fa-chevron-right"></i></span>
            </div>          
          </div>
        </div>
      </div>
    </div>

    <div class="col-sm-12">
      <div class="home-block-news">
        
        <div id="news-slider" class="owl-carousel news-carousel">

          <?php 
          
          $latest_news = get_element();

          while($latest_news->have_posts()) : $latest_news->the_post(); ?>
            
              <div> 
                <div class="row">
                  <div class="col-xs-5">
                    <div class="news-image">
                      <?php the_post_thumbnail('width=300&height=300&crop=1'); ?>
                    </div>
                  </div>
                  <div class="col-xs-11">
                    <div class="news-text">
                      <p class="date"><?php the_date(); ?></p>
                      <h3><?php the_title(); ?></h3>
                      <div class="sep"><hr></div>
                      <p><?php echo wp_trim_words(get_the_excerpt(), 20); ?></p>
                      <a href="<?php the_permalink(); ?>" class="button btn-1b">Read More</a>
                    </div>
                  </div>
                </div>
              </div>

            <?php endwhile; wp_reset_postdata(); ?>
            
        </div>
      </div>
    </div>

  </div>
</section>