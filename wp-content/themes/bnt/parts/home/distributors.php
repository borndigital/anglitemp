<?php 
  $branches = bnt_get_branches();
?>
<section>
  <div class="row hidden-xs">
    <div class="col-sm-16">
      <div class="home-distributors">
        <h2>From our door to yours within 24 hours.</h2>
        <div class="row">
          <div class="col-sm-offset-4 col-sm-8">
            <h3>Our locations</h3>
            <div class="locations row">
              <?php $i = 0; while ($branches->have_posts()) : $branches->the_post(); ?>
                <?php
                  $second_of_three = ($i % 3 === 1);
                  $number = get_field('phone_number', get_the_ID());
                ?>
                <div class="<?php echo $second_of_three ? 'col-sm-6' : 'col-sm-5' ?>">
                  <a href="<?php echo the_permalink(); ?>">
                    <i class="fa fa-map-marker"></i>
                    <span class="bigger"><?php echo get_the_title(); ?></span>
                    <span class="smaller"><?php echo $number; ?></span>
                  </a>
                </div>
              <?php $i++; endwhile; wp_reset_postdata(); ?>
            </div>
          </div>
          <div class="col-sm-3 areas-covered">
            <h3>Areas covered</h3>
            <p>
              UK. <br>
              N. Ireland <br>
              South of Ireland
            </p>
          </div>
        </div>
        <a href="/contact/" class="button">Contact us</a>
      </div>
    </div>
  </div>
</section>
