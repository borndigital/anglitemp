<section>
  <div class="row">
  
    <div class="col-sm-16">
      <div class="clients">

        <div class="client-controls">

          <div class="row">
            <div class="col-xs-16">
              Our<br>
              <span class="big">Preferred Suppliers</span>
              <hr>
            </div>
            <div class="col-xs-16">
              <div class="controls">
                <span class="client-prev"><i class="fa fa-chevron-left"></i></span>
                <span class="client-next"><i class="fa fa-chevron-right"></i></span>
              </div>
            </div>
          </div>
        </div>

        <div id="client-slider" class="owl-carousel client-carousel">


        </div>
      </div>
    </div>

  </div>
</section>
