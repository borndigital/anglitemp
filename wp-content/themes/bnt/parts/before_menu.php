<?php
  $branches = bnt_get_branches();
?>
<div class="phone-numbers hidden-xs">
  <div class="container">
    <ul class="row">
      <?php while ($branches->have_posts()) : $branches->the_post(); ?>
        <li class="col-sm-3">
          <a href="<?php echo the_permalink(); ?>">
            <?php echo get_the_title(); ?>
          </a>
        </li>
      <?php endwhile; wp_reset_postdata(); ?>
    </ul>
  </div>
</div>
