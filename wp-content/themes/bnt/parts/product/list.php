<?php
  $search = isset($_GET['search_product']) ? $_GET['search_product'] : false;
  $products = ang_get_products($search, 8);
?>
<?php while($products->have_posts()) : $products->the_post(); ?>
  <?php get_template_part('parts/product/item'); ?>
<?php endwhile; wp_reset_postdata(); ?>
