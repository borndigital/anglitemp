<?php if (get_field('supplier_testimonials')) { 

  $testimonial = get_field('supplier_testimonials');

  ?>
  <section class="supplier-testimonial hidden-xs">

    <div class="row">

      <div class="col-sm-4">
        <div class="title dark">
          <div class="header">
            <p class="big">Supplier</p>
            <p class="small">Testimonial</p>
            <div class="sep"><hr></div>
          </div>
        </div>
      </div>

      <div class="col-md-4 col-sm-5">
        <div class="testimonial-image">
          <img src="<?php the_field('image', $testimonial); ?>">
        </div>
      </div>

      <div class="col-sm-8">
        <div class="testimonial">
          <blockquote>
            <?php the_field('quote', $testimonial); ?>
          </blockquote>
        </div>
      </div>
    </div>

  </section>
<?php } ?>