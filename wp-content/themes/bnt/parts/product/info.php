<?php
  $product_id = get_the_ID();
  $prices = get_field('related_prices');
?>
<section class="product-intro">
  <div class="row">
    <div class="col-sm-4">
      <div class="title">
        <div class="header">
          <p class="big">
            <?php the_field('first_line', $product_id); ?>
          </p>
          <p class="small">
            <?php the_field('second_line', $product_id); ?>
          </p>
          <div class="sep"><hr></div>
          <p class="medium">
            <?php the_field('category', $product_id); ?>
          </p>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="copy">
        <?php the_content(); ?>
      </div>
    </div>
  </div>
</section>

<?php if ($prices != false): ?>
  <section class="product-info-alt">
    <div class="row">
      <div class="col-sm-4">
        <div class="title dark">
          <div class="header">
            <p class="big">Further</p>
            <p class="small">Information</p>
            <div class="sep"><hr></div>
          </div>
          <div class="controls">
            <div class="controls-spacer"></div>
            <span class="projects-prev"><i class="fa fa-chevron-left"></i></span>
            <span class="projects-next"><i class="fa fa-chevron-right"></i></span>
          </div>
        </div>
      </div>
      <div class="col-sm-12 product-prices">
        <div class="copy">
          <div class="row">
            <div id="product-prices-slider" class="paginated owl-carousel">
              <?php foreach($prices as $price): ?>
                <div class="col-sm-4 product-price">
                  <div class="image">
                    <img src="<?php the_field('brand_logo_large', $price->ID); ?>" alt="">
                  </div>
                  <a href="<?php the_field('brand_website', $price->ID); ?>" class="brand-website" target="_blank">
                    visit website
                  </a>
                  <a href="<?php the_field('attachment', $price->ID); ?>" class="button" target="_blank">
                    <?php echo substr($price->post_title, 0, 22); ?>
                    <i class="fa fa-file-pdf-o"></i>
                  </a>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>
