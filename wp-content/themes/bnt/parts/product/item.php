<div class="product">
  <?php the_post_thumbnail( 'width=240&height=220&crop=1', array('class' => 'product-image') ); ?>
  <div class="title-bg">
    <div class="title">
      <div class="title-inner">
        <?php the_title(); ?>
      </div>
    </div>
  </div>
  <div class="description">
    <h3 class="hidden-xs"><?php the_title(); ?></h3>
    <?php the_excerpt(); ?>
    <a class="button" href="<?php the_permalink(); ?>">More Info</a>
  </div>
</div>
