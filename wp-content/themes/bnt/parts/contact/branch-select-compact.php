<?php
  $branches = bnt_get_branches();
  $contact_page = bnt_get_contact_page();
?>
<section class="branch-select branch-select-compact">
  <div class="row">
    <div class="col-sm-4">
      <ul class="nav nav-tabs">
        <li role="presentation" class="dropdown">
          <a class="dropdown-toggle button" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
            <span class="current-branch">Change branch</span>
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/contact" data-branch-id="0">Head office</a></li>
            <?php $i = 1; while ($branches->have_posts()) : $branches->the_post(); ?>
              <li>
                <a href="<?php echo the_permalink(); ?>" data-branch-id="<?php the_ID(); ?>">
                  <?php echo get_the_title(); ?>
                </a>
              </li>
            <?php $i++; endwhile; wp_reset_postdata(); ?>
          </ul>
        </li>
      </ul>
    </div>
    <div class="col-sm-12">
      <p class="branch-summary">
        Get in touch with our <strong class="branch-title">Head office</strong> branch
        on <strong class="branch-telephone"><?php the_field('phone_number', $contact_page->ID); ?></strong> to find out more or order this product.
        All of our quality products can be delivered throughout the uk within
        <strong>24 hours</strong>
      </p>
    </div>
  </div>
</section>
