<?php
  $branches = bnt_get_branches();
?>
<section class="branch-select">
  <ul class="nav nav-tabs">
    <li role="presentation" class="dropdown">
      <a class="dropdown-toggle button" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
        Choose branch <span class="caret"></span>
      </a>
      <ul class="dropdown-menu" role="menu">
        <li><a href="/contact">Head office</a></li>
        <?php while ($branches->have_posts()) : $branches->the_post(); ?>
          <li>
            <a href="<?php echo the_permalink(); ?>">
              <?php echo get_the_title(); ?>
            </a>
          </li>
        <?php endwhile; wp_reset_postdata(); ?>
      </ul>
    </li>
  </ul>
</section>
