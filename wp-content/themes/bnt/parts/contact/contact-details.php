<?php
  $title = get_page_template_slug() != false ? 'Head' : get_the_title();
?>
<section class="contact-details">

  <div class="row">
    <div class="col-sm-4">
      <div class="title dark">
        <p class="big"><?php echo $title ?></p>
        <p class="small">Office</p>
        <div class="sep"><hr></div>
      </div>
    </div>

    <div class="col-sm-12">
      <div class="copy">
        <div class="row">
          <div class="col-sm-16 col-md-8">
            <div class="branch-details">
              <p class="telephone-number">
                Tel: <?php the_field('phone_number'); ?>
              </p>
              <div class="opening-hours">
                <h3>Opening hours</h3>
                <?php while ( have_rows('opening_hours') ) : the_row(); ?>
                  <div class="row">
                    <div class="col-sm-6 col-md-10 days">
                      <?php the_sub_field('days'); ?>
                    </div>
                    <div class="col-sm-10 col-md-6 times">
                      <?php the_sub_field('times'); ?>
                    </div>
                  </div>
                <?php endwhile; ?>
              </div>
              <h3>Email</h3>
              <p>
                <a href="mailto:<?php echo antispambot(get_field('email')); ?>">
                  <?php the_field('email'); ?>
                </a>
              </p>
            </div>
          </div>
          <div class="col-sm-8 hidden-xs hidden-sm">
            <img src="<?php the_field('image'); ?>"> 
          </div>
        </div>  
      </div>
    </div>

  </div>

</section>