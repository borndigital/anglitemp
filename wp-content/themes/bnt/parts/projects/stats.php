<div class="row">
  <div class="col-xs-8">
    <div class="row">
      <div class="col-sm-8">
        <div class="client">
          <div class="top">
            <br>
            <span class="bigger-stat">
              <?php the_field('project_value'); ?>
            </span>
          </div>
          <div class="sep"><hr></div>
          <span class="type">Project value</span>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="location">
          <div class="top">
            <i class="fa fa-map-marker fa-3x"></i> 
            <?php the_field('project_location'); ?>
          </div>
          <div class="sep"><hr></div>
          <span class="type">Location</span>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xs-8">
    <div class="row">
      <div class="col-sm-8">
        <div class="date">
          <div class="top">
            <?php
            // date is stored yyyymmdd
            $date = get_field('project_date');
            $month = __('September');
            $year = 2013;

            if ($date != false) {
              $date = strtotime($date);
              $month = date('M', $date);
              $year = date('Y', $date);
            }
            ?>
            <?php echo $month; ?> <br>
            <span class="big">
              <?php echo $year; ?>              
            </span>
          </div>
        </div>
        <div class="sep"><hr></div>
        <span class="type">Date</span>
      </div>
      <div class="col-sm-8">
        <div class="quantity">
          <div class="top">
            <span class="big"><?php the_field('value'); ?></span><br>
            <?php the_field('measurement'); ?>
          </div>
        </div>
        <div class="sep"><hr></div>
        <span class="type"><?php the_field('stat_name'); ?></span>
      </div>
    </div>
  </div>
</div>
