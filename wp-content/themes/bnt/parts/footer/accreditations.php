<div class="col-sm-8 hidden-xs">
  <h3 class="accred-title">Membership &amp; Accreditations</h3>
  <div class="sep accred-hr"><hr></div>
  <div class="accreditations">
    <div class="row">
      <div class="col-md-16 first-row">
        <span>
          <a href="http://www.tica-acad.co.uk/index.php/homepage">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/footer/tica.png" alt="TICA Associate Member">
          </a>
        </span>
        <span>
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/footer/iso.png" alt="ISO 9001 Bureau Veritas Certification">
        </span>
        <span>
          <a href="https://www.linkedin.com/company/3666877?trk=vsrp_companies_res_name&trkInfo=VSRPsearchId%3A3226404301438866166291%2CVSRPtargetId%3A3666877%2CVSRPcmpt%3Aprimary">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/footer/linkedin.png" alt="Linkedin">
          </a>
        </span>
        <span>
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/footer/necc.png" alt="Accreditation">
        </span>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-16">
        <span>
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/footer/roo.png" alt="Accreditation">
        </span>
      </div>
    </div>
  </div>
</div>
