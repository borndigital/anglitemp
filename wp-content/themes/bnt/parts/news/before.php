<?php
  $products = get_recently_updated_price_list_products('2 weeks ago');
?>
<section class="price-updates">
  <div class="row">
    <div class="col-sm-4">
      <div class="title">
        News &amp; Updates
      </div>
    </div>
    <div class="col-sm-12">
      <ul>
        <?php while ($products->have_posts()) : $products->the_post(); ?>
        <li>
          <a href="/pricelists" class="price-title">
            <?php echo the_title(); ?>
          </a>
          <p class="price-update-text">
            <?php if (get_field('update_text')): ?>
              <span><?php echo the_field('update_text'); ?></span>
            <?php endif ?>
            <a href="/pricelists" class="learn-more">Learn more</a>
          </p>
        </li>
        <?php endwhile; wp_reset_postdata(); ?>
      </ul>
    </div>
  </div>
</section>
