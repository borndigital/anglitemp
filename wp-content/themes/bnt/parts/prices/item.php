<?php
  $id = get_the_ID();
  $image = get_field('image', $id);
  $brand_logo = get_field('brand_logo', $id);
  $category = get_field('category', $id);
  $attachment = get_field('attachment', $id);

  $modified = strtotime($post->post_modified_gmt);
  $two_weeks_ago = strtotime('-2 week');

  // MIKE: We're using 2 hours for the time being
  $two_weeks_ago = strtotime('-2 hour');
?>
<li class="clearfix">
  <div class="row">
    <div class="col-sm-4 image">
      <img src="<?php echo $image ?>" alt="<?php echo the_title(); ?>" />

      <?php if ($modified > $two_weeks_ago): ?>
        <div class="recently-updated">
          <i class="fa fa-star"></i>
          Recently updated
        </div>
      <?php endif ?>
    </div>
    <div class="col-sm-offset-1 col-sm-6 title">
      <h2><?php echo the_title(); ?></h2>
      <p class="category"><?php echo $category; ?></p>
      <p class="last-updated">Last updated: <?php echo date('d/m/Y', $modified); ?></p>
    </div>
    <div class="col-sm-5 extra">
      <div class="brand-logo">
        <img src="<?php echo $brand_logo ?>" />
      </div>
      <a href="<?php echo $attachment ?>" class="button attachment-button" target="_blank">
        Price list
        <i class="fa fa-arrow-circle-o-down"></i>
      </a>
    </div>
  </div>
</li>
