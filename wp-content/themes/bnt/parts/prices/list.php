<?php
  $search = isset($_GET['price_search']) ? $_GET['price_search'] : false;
  $products = get_price_list($search);
?>
<ul class="price-list">
  <?php while ($products->have_posts()) : $products->the_post(); ?>
    <?php get_template_part('parts/prices/item'); ?>
  <?php endwhile; wp_reset_postdata(); ?>
</ul>
