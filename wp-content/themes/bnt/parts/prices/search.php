<section>
  <div class="row">
    <div class="col-sm-16">
      <form action="" method="GET" class="price-list-search search">
        <div class="input-wrap">
          <input type="text" name="price_search" id="search" placeholder="search">
          <input type="submit" value="Search">
          <i class="fa fa-search"></i>
        </div>
        <p>Price lists which have been recently updated are highlighted.</p>
      </form>
    </div>
  </div>
</section>
