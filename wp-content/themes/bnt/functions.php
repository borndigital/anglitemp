<?php

function bnt_enqueue_scripts() {
  wp_enqueue_style( 'anglitemp-theme', get_template_directory_uri() . '/style.css' );
  wp_enqueue_style( 'bnt-theme', get_stylesheet_uri() );

  wp_enqueue_script( 'bnt-price-list', get_stylesheet_directory_uri() . '/js/price-list.js', array('jquery', 'main'), null, true);
  wp_enqueue_script( 'bootstrap-min-js', get_stylesheet_directory_uri() . '/js/bootstrap.min.js', array('jquery'), null, true);

  if (is_front_page()) {
    wp_enqueue_script( 'bnt-product-search', get_stylesheet_directory_uri() . '/js/product-search.js', array('jquery', 'main'), null, true);
  }
}
add_action( 'wp_enqueue_scripts', 'bnt_enqueue_scripts' );

function bnt_client_labels($labels) {
  return array(
    'name' => 'Suppliers',
    'singular_name' => 'Supplier',
    'add_new' => 'Add New',
    'add_new_item' => 'Add New Supplier',
    'edit_item' => 'Edit Supplier',
    'new_item' => 'New Supplier',
    'all_items' => 'All Suppliers',
    'view_item' => 'View Suppliers',
    'search_items' => 'Search Suppliers',
    'not_found' =>  'No Suppliers found',
    'not_found_in_trash' => 'No Suppliers found in Trash', 
    'parent_item_colon' => '',
    'menu_name' => 'Suppliers'
  );
}
add_action('ang_client_labels', 'bnt_client_labels');

function bnt_project_information_acf_fields($fields) {
  $filtered_fields = array();

  // add our new field to the start
  $filtered_fields[] = array (
    'key' => 'field_53a412002cfa1',
    'label' => 'Project value',
    'name' => 'project_value',
    'type' => 'text',
    'default_value' => '',
    'placeholder' => '',
    'prepend' => '',
    'append' => '',
    'formatting' => 'html',
    'maxlength' => '',
  );

  $filtered_fields[] = array (
    'key' => 'field_53a412002cfaa',
    'label' => 'Location',
    'name' => 'project_location',
    'type' => 'text',
    'default_value' => '',
    'placeholder' => '',
    'prepend' => '',
    'append' => '',
    'formatting' => 'html',
    'maxlength' => '',
  );

  $ignored_fields = array('application', 'spec', 'insulation_product');

  foreach ($fields as $field) {
    // echo '<pre>';
    // var_dump($field);
    // echo '</pre>';

    if (!in_array($field['name'], $ignored_fields)) {
      $filtered_fields[] = $field;
    }
  }

  // exit;
  
  return $filtered_fields;
}

function ang_after_setup_theme()
{
  // our parent theme registers contact meta for this page
  // we're doing it in branches instead
  remove_action('init', 'ang_register_contact_meta', 123);

  // we don't associate projects with clients here
  remove_action('init', 'ang_register_project_client_meta', 200);

  add_filter('project_information_acf_fields', 'bnt_project_information_acf_fields');
}
add_action( 'after_setup_theme', 'ang_after_setup_theme', 1) ;
