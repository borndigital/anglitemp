        <footer>
            
            <div class="container">
                
                <div class="row">
                    
                    <?php get_template_part( 'parts/footer/address' ); ?>

                    <?php get_template_part( 'parts/footer/accreditations' ); ?>

                    <?php get_template_part( 'parts/footer/links' ); ?>

                </div>

                <div class="row">

                    <div class="col-md-16">
                        <div class="copyright">
                            Bold Nu-Tec Insulation Products &amp; Services Limited, Registered in England &amp; Wales. 
                            Company Number: <?php the_field('company-number', 'option'); ?>
                        </div>

                    </div>

                </div>
            </div>

        </footer>
        <?php wp_footer(); ?>
    </body>
</html>