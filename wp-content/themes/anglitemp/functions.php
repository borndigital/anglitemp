<?php

/* Include template specific meta */
include_once('meta/contact.php');
include_once('meta/header-image.php');
include_once('meta/product-title.php');
include_once('meta/our-company.php');

add_theme_support( 'post-thumbnails' );

function ang_register_scripts() {
    wp_register_script('main', get_template_directory_uri().'/js/main.js', array('jquery'), null, true);
    wp_register_script('black-white', get_template_directory_uri().'/js/BlackAndWhite.js', array('jquery'), null, true);
    wp_register_script('carousel', get_template_directory_uri().'/js/owl.carousel.min.js', array('jquery'), null, true);
    wp_register_script('map', get_template_directory_uri().'/js/jquery.vmap.js', null, null, true);
    wp_register_script('map-world', get_template_directory_uri().'/js/jquery.vmap.world.js', null, null, true);
    wp_register_script('map-init', get_template_directory_uri().'/js/jquery.map.init.js', null, null, true);
    wp_register_script('select', get_template_directory_uri().'/js/jquery.customSelect.latest.min.js', null, null, true);
    wp_register_script('modernizr', get_template_directory_uri().'/js/modernizr-2.8.0.min.js', null, null, false);
}
add_action( 'init', 'ang_register_scripts' );

function ang_enqueue_scripts() {
    wp_enqueue_script('jquery');
    wp_enqueue_script('modernizr');

    if (is_post_type_archive('project') || is_tax('countries') || is_tax('type') || is_singular('project')) {
        wp_enqueue_script('map');
        wp_enqueue_script('map-world');
        wp_enqueue_script('map-init');

        // Pass through array of countries
        $countries = get_countries();

        if (is_tax('countries')) {
            $country = get_queried_object();
            $current_country = $country->slug;
        }

        wp_localize_script('map-init', 'ang_script_vars', array(
            'countries' => $countries,
            'current_country' => $current_country,
        ));
    }

    wp_enqueue_script('black-white');
    wp_enqueue_script('select');
    wp_enqueue_script('carousel');
    wp_enqueue_script('main');
}
add_action( 'wp_enqueue_scripts', 'ang_enqueue_scripts' );

function ang_main_stylesheet() {
    wp_register_style( 'main', get_stylesheet_directory_uri().'/style.css' );
    wp_enqueue_style( 'main' );
}
add_action( 'wp_enqueue_scripts', 'ang_main_stylesheet' );

function ang_google_analytics() {
    ?>
    <!-- Google Analytics -->
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-132853810-1', 'auto');
    ga('send', 'pageview');
    </script>
    <!-- End Google Analytics -->
    <?php
}
add_action( 'wp_head', 'ang_google_analytics' );

function add_ie_html5_shim () { ?>
    <!--[if lt IE 9]>
    	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url'); ?>/css/ie.css"/>
    <![endif]-->
<?php
}
add_action('wp_head', 'add_ie_html5_shim');

function media_queries_ie() { ?>

    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<?php }
add_action('wp_footer', 'media_queries_ie');

register_nav_menus( array(
	'main_menu' => 'Main Menu',
	'footer_menu' => 'Footer Menu'
) );

add_filter("gform_confirmation_anchor", create_function("","return true;"));


//Custom Walker for the main menu
class Ang_Walker extends Walker_Nav_Menu
{
    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

        global $wp_query;

        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $class_names = $value = '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
        $class_names = ' class="' . esc_attr( $class_names );

        $class_names .= '"';

        $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

        $item_output = '';

        if (0 == $depth) {
            $item_output .= '<a'. $attributes .'>';
            $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
            $item_output .= '</a>';
        } else {
            $item_output = $args->before;
            $item_output .= '<div class="item"><a'. $attributes .'"><h3>';
            $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
            $item_output .= '</h3></a><p>' . $item->post_content. '</p></item><a href="' . $item->url . '">> Read More</a><hr></div>'; /* This is where I changed things. */
            $item_output .= $args->after;
        }

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }

}

function get_element($type = 'post', $number = 3, $order = 'DESC') {

    $args = array(
      'post_status' => 'publish',
      'post_type'   => $type,
      'showposts'   => $number,
      'orderby'     => 'menu_order date',
      'order'       => $order,

   );

   $latest = new WP_Query($args);

   return $latest;

}

function ang_product_case_studies($number = -1, $order = 'DESC') {
  $projects = get_field('case_studies');
  $ids = array('-1');
  foreach ($projects as $project) {
    array_push($ids, $project->ID);
  }

  $args = array(
      'post_status' => 'publish',
      'post_type'   => 'project',
      'showposts'   => $number,
      'orderby'     => 'menu_order date',
      'order'       => $order,
      'post__in'    => $ids
   );

   $latest = new WP_Query($args);

   return $latest;
}


function ang_reorder_post_types( $post_types ) {
    $post_types = array( 'project', 'product', 'branches' );
    return $post_types;
}
add_filter( 'metronet_reorder_post_types', 'ang_reorder_post_types' );

function ang_overide_bg() {

    $home_page_id = get_option('page_on_front');

    if (is_home() || is_singular('post')) {
        $page_id = get_option('page_for_posts');
    } elseif (is_post_type_archive() || is_singular()) {
        $page_id = linked_page();
    } elseif (is_page()) {
        $page_id = get_the_ID();
    }

    $header_image = get_post_meta($page_id, 'header_background' , true);

    if (empty($header_image)) {
        $header_image = get_post_meta($home_page_id, 'header_background' , true);
    }

    $image = wp_get_attachment_url( $header_image );

    echo 'style="background-image: url(' . $image . ');"';

}

function the_slug(){
    $slug = basename(get_permalink());
    $slug = apply_filters('slug_filter', $slug);
    echo $slug;
}

function get_the_slug(){
    $slug = basename(get_permalink());
    $slug = apply_filters('slug_filter', $slug);
    return $slug;
}

?>
