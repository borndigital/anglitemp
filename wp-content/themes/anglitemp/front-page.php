<?php get_header(); ?>

<div class="container">
	
	<?php get_template_part('parts/home/products'); ?>

	<?php get_template_part('parts/home/clients'); ?>

	<?php get_template_part('parts/home/projects'); ?>

	<?php get_template_part('parts/home/news'); ?>	

</div>

<?php get_footer(); ?>