<?php get_header(); ?>

<div class="container">

	<?php get_template_part('parts/product/info'); ?>

	<?php get_template_part('parts/product/performance'); ?>

	<?php get_template_part('parts/product/case-studies'); ?>

	<?php get_template_part('parts/product/testimonial'); ?>

</div>

<?php get_footer(); ?>