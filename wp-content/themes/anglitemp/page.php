<?php get_header(); ?>

<div class="container">
	
	<section>

		<div class="row">

			<div class="col-sm-4">

				<div class="page-title">
				
					<h1><?php the_title(); ?></h1>

					<hr>

				</div>

			</div>

			<div class="col-sm-12">

				<div class="content">

					<div class="top"></div>

					<?php the_content(); ?>

				</div>

			</div>

		</div>

	</section>

</div>

<?php get_footer(); ?>