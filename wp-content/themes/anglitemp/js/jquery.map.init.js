jQuery(window).load(function() {

	var countries = ang_script_vars.countries;
	var current = ang_script_vars.current_country;

	jQuery('#vmap').vectorMap({
		map: 'world_en',
		backgroundColor: '#FFFFFF',
		color: '#999999',
		hoverOpacity: 0.7,
		enableZoom: true,
		showTooltip: true,
		scaleColors: ['#FF0000', '#006491'],
		normalizeFunction: 'polynomial',
		onRegionClick: function (event, code, region) {	
			
			if(jQuery.inArray(code, countries) > -1) {
				window.location.href='/countries/' + code + '/';
			}
			
		},

		onRegionOver: function (event, code, region) {

			if(jQuery.inArray(code, countries) > -1) {
				document.body.style.cursor = "pointer";
			}
		},

		onRegionOut: function (element, code, region) {

			if(jQuery.inArray(code, countries) > -1) {
				document.body.style.cursor = "default";
			}

		}
	});

	var country_colors = {};

	// Set the colours of the available countries
	jQuery.each( countries, function( index, value ){
    	country_colors[value] = '#1177BC';
	});
	
	jQuery('#vmap').vectorMap('set', 'colors', country_colors);

	// Set the colour of the selected country
	jQuery.each( countries, function( index, value ){
    	country_colors[current] = '#EC130A';
	});

	jQuery('#vmap').vectorMap('set', 'colors', country_colors);

});

jQuery(document).ready(function($) {

	// set the size and position of the map
	if ( $('.map-container').length ) {
		$('.map').height($('.map').width() * 0.7);
	}

	$(window).smartresize(function(){
		if ( $('.map-container').length ) {
			$('.map').height($('.map').width() * 0.7);
		}
	});


});