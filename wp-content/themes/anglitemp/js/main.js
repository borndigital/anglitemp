(function($) {

  function branch_select_compact() {
    $('.branch-select-compact').each(function() {
      var current_branch = $(this).find('.current-branch'),
          dropdown_toggle = $(this).find('.dropdown-toggle'),
          branch_summary = $(this).find('.branch-summary'),
          branch_title = $(this).find('.branch-title'),
          branch_telephone = $(this).find('.branch-telephone');

      if (branch_summary.hasClass('hidden')) {
        branch_summary.hide().removeClass('hidden');
      }

      $(this).on('click', '.dropdown-menu a', function(e) {
        e.preventDefault();
        var branch_id = $(this).attr('data-branch-id');

        // hide the dropdown
        dropdown_toggle.dropdown('toggle');

        // update the selected branch
        current_branch.text($(this).text());

        branch_summary.stop().fadeOut();

        // get the branch
        get_branch(branch_id, function(response) {
          branch_summary.stop().fadeIn();
          branch_title.text(response.title.replace('Contact', 'Head office'));
          branch_telephone.text(response.telephone);
        });

        return false;
      });
    });
  }

  function get_branch(id, callback) {
    $.ajax({
      url : '/api/branch/',
      data: {
        id: id
      },
      success: callback
    });
  }

  $(document).ready(function($) {

      if ($('.branch-select-compact').length) {
        branch_select_compact();
      }

      // Make sure the boxes on the homepage are the correct size
      $(window).smartresize(function() {
          var product_height = $( ".product-image" ).outerHeight();
          $( ".product-range" ).outerHeight(product_height);
      });

      // When you click a product, reveal the description
      $(document).on('click', '.product', function() {

          var this_desc = $(this).find(".description");

          $( ".description" ).not(this_desc).fadeOut();

          if (!$( this_desc ).is(':visible')) {
              this_desc.fadeIn();
          }

      });

      // We don't know how many results we'll have on the projects page so make sure the search bar is the right height
      width = $(document).width();
      if (width > 767) {
          var results_height = $( ".results" ).outerHeight();
          var page_height = $( ".content" ).outerHeight();
          var comparison_height = $( ".comparisons" ).outerHeight();
          $( ".search-results" ).outerHeight(results_height);
          $( ".page-title" ).outerHeight(page_height);
          $( ".performance" ).outerHeight(comparison_height);
      } else {
          $( ".search-results" ).outerHeight('auto');
          $( ".page-title" ).outerHeight('auto');
          $( ".performance" ).outerHeight('auto');
      }

      $(window).smartresize(function() {

          width = $(document).width();

          if (width > 767) {
              var results_height = $( ".results" ).outerHeight();
              var page_height = $( ".content" ).outerHeight();
              var comparison_height = $( ".comparisons" ).outerHeight();
              $( ".search-results" ).outerHeight(results_height);
              $( ".page-title" ).outerHeight(page_height);
              $( ".performance" ).outerHeight(comparison_height);
          } else {
              $( ".search-results" ).outerHeight('auto');
              $( ".page-title" ).outerHeight('auto');
              $( ".performance" ).outerHeight('auto');
          }
      });

      // Due to the layout of the contact form we need to make sure that the positioning of the elements is different on mobile & desktop
      fix_contact_form();

      $(window).smartresize(function() {
          fix_contact_form();
      });

      setTimeout(function() {

          $(".icon").each(function(index) {
              $(this).delay(600*index).animate({opacity: 1}, 800);
          });

      }, 400);

      // Show/Hide mobile menu
      $( ".mobile-menu-button" ).click(function() {
          $( ".mobile-menu" ).slideToggle();
      });

      // Each button has some divs appended for the animated border
      $( ".egg" ).append( "<div class='egg-top'></div><div class='egg-left'></div><div class='egg-right'></div><div class='egg-bottom'></div>" );

      // Animate the border of the buttons
      $('.egg').mouseover(function() {
          var width = $(this).outerWidth();
          var height = $(this).outerHeight();

          $('.egg-top, .egg-bottom').stop().animate({width: width}, 500);
          $('.egg-left, .egg-right').stop().animate({height: height}, 500);
      });

      $('.egg').mouseout(function() {
          $('.egg-top, .egg-bottom').stop().animate({width:'0'}, 500);
          $('.egg-left, .egg-right').stop().animate({height:'0'}, 500);
      });

      // Clients Slider
      var client_slider = $("#client-slider");

      client_slider.owlCarousel({
          itemsTablet : [768,3],
          itemsTabletSmall : [600,1],
          itemsMobile : [479,1],
          pagination : false,
          autoPlay : 5000,
          jsonPath : '/api/clients',
          jsonSuccess : customDataSuccess
      });

      $(".client-next").click(function() {
          client_slider.trigger('owl.next');
      });

      $(".client-prev").click(function() {
          client_slider.trigger('owl.prev');
      });

      function customDataSuccess(data) {
          var content = "";
          for (var i in data["items"]) {

             var img = data["items"][i].img;
             var alt = data["items"][i].alt;

             content += "<img src=\"" +img+ "\" alt=\"" +alt+ "\" width=\"112\" height=\"72\">"
          }
          $(client_slider).html(content);
      }

      // Projects Slider
      var projects_slider = $("#projects-slider");

      projects_slider.owlCarousel({
          singleItem: true,
          pagination : false,
          slideSpeed : 800,
          afterAction : syncPosition,

      });

      var projects_slider_name = $("#projects-slider-name");

      projects_slider_name.owlCarousel({
          singleItem: true,
          transitionStyle : 'fade',
          pagination : false,
          mouseDrag : false,
          touchDrag : false

      });

      $(".projects-next").click(function() {
          projects_slider.trigger('owl.next');
          projects_slider_name.trigger('owl.next');
      });

      $(".projects-prev").click(function() {
          projects_slider.trigger('owl.prev');
          projects_slider_name.trigger('owl.prev');
      });

      function product_prices_slider()
      {
        var product_price_slider = $("#product-prices-slider");

        product_price_slider.owlCarousel({
            transitionStyle : 'fade',
            pagination : false,
            mouseDrag : false,
            touchDrag : false,
            items : 4
        });

        $(".projects-next").click(function() {
            projects_slider.trigger('owl.next');
            product_price_slider.trigger('owl.next');
        });

        $(".projects-prev").click(function() {
            projects_slider.trigger('owl.prev');
            product_price_slider.trigger('owl.prev');
        });
      }

      if ($("#product-prices-slider").length) {
        product_prices_slider();
      }

      function syncPosition() {
          var current = this.currentItem;
          //alert(current);
      }

      // News Slider
      var news_slider = $("#news-slider");

      news_slider.owlCarousel({
          singleItem: true,
          pagination : false,
          slideSpeed : 500,
      });

      $(".news-next").click(function() {
          news_slider.trigger('owl.next');
      });

      $(".news-prev").click(function() {
          news_slider.trigger('owl.prev');
      });

      // Personnel Slider
      var personnel_slider = $("#personnel-slider");

      personnel_slider.owlCarousel({
          pagination : false,
          items : 3,
          itemsDesktop : [1199,3],
          itemsDesktopSmall : [979,2],
          itemsMobile : [479,2],
          slideSpeed : 500,
      });

      $(".personnel-next").click(function() {
          personnel_slider.trigger('owl.next');
      });

      $(".personnel-prev").click(function() {
          personnel_slider.trigger('owl.prev');
      });


      // Select box on Project Page
      var $cs = $('.search-by-type').customSelect();

      $('select[name=type]').change(function() {
          window.location = $(this).val();
      });
  });

  $(window).load(function() {
    // Black and white image filter for products
    monochrome_products();

    // fix product heights
    var product_height = $( ".product-image" ).outerHeight();
    $( ".product-range" ).outerHeight(product_height);
  });

  function fix_contact_form() {

      width = $(document).width();

      if (width < 767) {
          $( "#field_1_5" ).insertAfter( $( "#field_1_4" ) );
      } else {
          $( "#field_1_5" ).insertBefore( $( "#field_1_1" ) );
      }
  }

  /**
   * Products need to be monochrome
   */
  function monochrome_products() {
      var is_touch_device = 'ontouchstart' in document.documentElement;

      if (!is_touch_device) {
          $('.product').BlackAndWhite({
              hoverEffect : true, // default true
              // set the path to BnWWorker.js for a superfast implementation
              webworkerPath : false,
              // for the images with a fluid width and height
              responsive:true,
              // to invert the hover effect
              invertHoverEffect: false,
              // this option works only on the modern browsers ( on IE lower than 9 it remains always 1)
              intensity:1,
              speed: { //this property could also be just speed: value for both fadeIn and fadeOut
                  fadeIn: 200, // 200ms for fadeIn animations
                  fadeOut: 800 // 800ms for fadeOut animations
              },
              onImageReady:function(img) {
                 $('.product-image').css('opacity', 1);
              }
          });
      } else {
          $('.product-image').css('opacity', 1);
      }
  }

  window.monochrome_products = monochrome_products;

  // debouncing function from John Hann
  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
  var debounce = function (func, threshold, execAsap) {
      var timeout;

      return function debounced () {
          var obj = this, args = arguments;
          function delayed () {
              if (!execAsap)
                  func.apply(obj, args);
              timeout = null;
          };

          if (timeout)
              clearTimeout(timeout);
          else if (execAsap)
              func.apply(obj, args);

          timeout = setTimeout(delayed, threshold || 100);
      };
  }
  // smartresize
  jQuery.fn['smartresize'] = function(fn) {
    return fn ? this.bind('resize', debounce(fn)) : this.trigger('smartresize');
  };

  // smartkeyup
  jQuery.fn['smartkeyup'] = function(fn, threshold) {
    return fn ? this.bind('keyup', debounce(fn, threshold)) : this.trigger('smartkeyup');
  };

})(jQuery);
