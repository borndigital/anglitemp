<?php get_header(); ?>

<div class="container">

	<?php while ( have_posts() ) : the_post(); ?>
		
		<section>

			<div class="row">

				<article class="single">
					<div class="col-sm-4 col-xs-16">
						<?php the_post_thumbnail( 'width=230&height=230&crop=1', array('class' => 'featured-image hidden-xs')); ?>
						<?php the_post_thumbnail( 'width=400&height=140&crop=1', array('class' => 'featured-image-small visible-xs') ); ?>
					</div>

					<div class="col-sm-12 col-xs-16">

						<div class="overview">
							<p class="date"><?php the_date(); ?></p>
							<h2><?php the_title(); ?></h2>
							<div class="sep"><hr></div>
							<p><?php the_content() ?></p>
							<a href="<?php bloginfo('url'); ?>/news/" class="button">Back to News</a>
						</div>

					</div>
				</article>

			</div>

		</section>

	<?php endwhile;  ?>

</div>

<?php get_footer(); ?>