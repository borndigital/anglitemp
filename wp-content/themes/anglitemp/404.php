<?php get_header(); ?>

<div class="container">
	
	<section>

		<div class="row">

			<div class="col-lg-4">

				<div class="page-title">
				
					<h1>404 - Error</h1>

					<hr>

				</div>

			</div>

			<div class="col-lg-12">

				<div class="content">

					<div class="top"></div>

					<h3>We're sorry but this page doesn't exist.</h3>

					<p>You can use the navigation at the top of the site to get to where you want to go.</p>

				</div>

			</div>

		</div>

	</section>

</div>

<?php get_footer(); ?>