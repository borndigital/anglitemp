// Require all packages
var gulp = require('gulp');
var less = require('gulp-less');
var notify = require('gulp-notify');
var livereload = require('gulp-livereload');
var autoprefixer = require('gulp-autoprefixer');
var imagemin = require('gulp-imagemin');

// Less Compiler
gulp.task('less', function() {

	return gulp
		.src('css/style.less')
        .pipe(less())
        .pipe(autoprefixer('last 2 version', "> 1%", "ie 8", "ie 7"))
        .pipe(gulp.dest('./'))
        .pipe(livereload())
        .pipe(notify({ message: 'Less Compiled'}));

});

// Image Optimiser
gulp.task('images-opt', function () {
    gulp.src('./images/*.*')
        .pipe(imagemin({optimizationLevel: 3, progressive: true, interlaced: true}))
        .pipe(gulp.dest('./images/'));
});

// Watcher
gulp.task('default', function() {

    gulp.watch('./css/*.less', function() {
        gulp.run('less');
    });

});
