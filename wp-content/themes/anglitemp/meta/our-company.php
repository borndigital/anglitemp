<?php

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_our-company',
		'title' => 'Our Company',
		'fields' => array (
			array (
				'key' => 'field_53b56c0c85ef3',
				'label' => 'Company Text',
				'name' => 'company_text',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'no',
			),
			array (
				'key' => 'field_53b56c2585ef4',
				'label' => 'Profile Text',
				'name' => 'profile_text',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'no',
			),
			array (
				'key' => 'field_53b56c3285ef5',
				'label' => 'Gordon\'s LinkedIn',
				'name' => 'gordons_linkedin',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_53b56c3e85ef6',
				'label' => 'Tom\'s LinkedIn',
				'name' => 'toms_linkedin',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_53b56c5c85ef7',
				'label' => 'Our sister company',
				'name' => 'history',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'no',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'templates/our-company.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'the_content',
			),
		),
		'menu_order' => 0,
	));

	register_field_group(array (
		'id' => 'acf_datasheet',
		'title' => 'Datasheet',
		'fields' => array (
			array (
				'key' => 'field_53b6765bf13a4',
				'label' => 'Our Company Datasheet',
				'name' => 'our_company_datasheet',
				'type' => 'file',
				'save_format' => 'url',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'templates/our-company.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'side',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
