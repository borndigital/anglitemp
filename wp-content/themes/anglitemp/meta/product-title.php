<?php

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_title-output',
		'title' => 'Title Output',
		'fields' => array (
			array (
				'key' => 'field_53a9461b0a0d0',
				'label' => 'First Line',
				'name' => 'first_line',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_53a946230a0d1',
				'label' => 'Second Line',
				'name' => 'second_line',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'product',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'side',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
