<?php

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_page-header',
		'title' => 'Page Header',
		'fields' => array (
			array (
				'key' => 'field_53a80d3b0a58a',
				'label' => 'Header Background',
				'name' => 'header_background',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'large',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
