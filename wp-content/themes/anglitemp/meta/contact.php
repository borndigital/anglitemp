<?php

function ang_register_contact_meta() {
  if(function_exists("register_field_group"))
  {
  	register_field_group(array (
  		'id' => 'acf_personnel',
  		'title' => 'Personnel',
  		'fields' => array (
  			array (
  				'key' => 'field_53a2e9c5e976d',
  				'label' => 'Personnel',
  				'name' => 'personnel',
  				'type' => 'repeater',
  				'sub_fields' => array (
  					array (
  						'key' => 'field_53a2e9cde976e',
  						'label' => 'Image',
  						'name' => 'image',
  						'type' => 'image',
  						'column_width' => '',
  						'save_format' => 'url',
  						'preview_size' => 'medium',
  						'library' => 'all',
  					),
  					array (
  						'key' => 'field_53a2e9d7e976f',
  						'label' => 'Name',
  						'name' => 'name',
  						'type' => 'text',
  						'column_width' => '',
  						'default_value' => '',
  						'placeholder' => '',
  						'prepend' => '',
  						'append' => '',
  						'formatting' => 'none',
  						'maxlength' => '',
  					),
  					array (
  						'key' => 'field_53a2e9dde9770',
  						'label' => 'Role',
  						'name' => 'role',
  						'type' => 'text',
  						'column_width' => '',
  						'default_value' => '',
  						'placeholder' => '',
  						'prepend' => '',
  						'append' => '',
  						'formatting' => 'none',
  						'maxlength' => '',
  					),
  					array (
  						'key' => 'field_53a2e9f9e9772',
  						'label' => 'E-Mail',
  						'name' => 'e-mail',
  						'type' => 'email',
  						'column_width' => '',
  						'default_value' => '',
  						'placeholder' => '',
  						'prepend' => '',
  						'append' => '',
  					),
  					array (
  						'key' => 'field_53a2e9e6e9771',
  						'label' => 'LinkedIn',
  						'name' => 'linkedin',
  						'type' => 'text',
  						'column_width' => '',
  						'default_value' => '',
  						'placeholder' => '',
  						'prepend' => '',
  						'append' => '',
  						'formatting' => 'none',
  						'maxlength' => '',
  					),
  				),
  				'row_min' => '',
  				'row_limit' => '',
  				'layout' => 'row',
  				'button_label' => 'Add Person',
  			),
  		),
  		'location' => array (
  			array (
  				array (
  					'param' => 'page',
  					'operator' => '==',
  					'value' => '53',
  					'order_no' => 0,
  					'group_no' => 0,
  				),
  			),
  		),
  		'options' => array (
  			'position' => 'normal',
  			'layout' => 'no_box',
  			'hide_on_screen' => array (
  			),
  		),
  		'menu_order' => 0,
  	));
  }
}
add_action('init', 'ang_register_contact_meta', 123);
