<!DOCTYPE html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php wp_title(); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico" />
        <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <header <?php ang_overide_bg(); ?>>

            <div class="mobile-menu hidden-sm hidden-md hidden-lg">
                <?php wp_nav_menu(array( 'container' => '', 'theme_location' => 'main_menu')); ?>
            </div>

            <div class="mobile-bar visible-xs">
                <div class="container">
                    <div class="col-xs-8">
                        <i class="fa fa-bars mobile-menu-button"> Menu</i>
                    </div>

                    <div class="col-xs-8">
                        <div class="phone">
                            <a href="tel:<?php the_field('phone', 'option'); ?>"><?php the_field('phone', 'option'); ?></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
        
                    <div class="col-sm-4">
                        <div class="logo">
                            <a href="<?php bloginfo('url'); ?>">Anglitemp - Industrial Insulation &amp; subsea systems.</a>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="menu hidden-xs">
                            <span class="phone">Call <?php the_field('phone', 'option'); ?></span>
                            <?php 

                            $walker = new Ang_Walker;
        
                            wp_nav_menu(array(
                                'echo' => true,
                                'container' => '',
                                'theme_location' => 'main_menu',
                                'walker' => $walker
                            ));

                            ?>
                        </div>
                    </div>
                </div>

                <?php if (is_front_page()) { ?>
                    <?php get_template_part( 'parts/home/intro'); ?>
                <?php } ?>


            </div>

        </header>