<?php get_header(); ?>

<div class="container">

	<?php get_template_part('parts/projects/results'); ?>

	<?php get_template_part('parts/projects/map'); ?>		

</div>

<?php get_footer(); ?>