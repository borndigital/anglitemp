<section>

	<div class="row">

		<div class="col-sm-16">
			<div class="products">
				<div class="product">
					<div class="product-range home-block-small">
						Our<br>
						<span class="big">Product</span><br>
						<span class="small">Range<br>
						<div class="sep"></div>
					</div>
				</div>

				<?php

				$products = get_element('product', 7, 'ASC');

				while($products->have_posts()) : $products->the_post(); ?>

					<div class="product">
						<?php the_post_thumbnail( 'width=240&height=220&crop=1', array('class' => 'product-image') ); ?>
						<div class="title-bg">
							<div class="title">
								<div class="title-inner">
									<?php the_title(); ?>
								</div>
							</div>
						</div>

						<div class="description">
							<hr>
							<h3 class="hidden-xs"><?php the_title(); ?></h3>
							<?php the_excerpt(); ?>
							<a class="button" href="<?php the_permalink(); ?>">More Info</a>
						</div>
					</div>

  				<?php endwhile; wp_reset_postdata(); ?>

			</div>
		</div>

	</div>
</section>
