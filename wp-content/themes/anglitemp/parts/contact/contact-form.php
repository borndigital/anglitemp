<section>

	<div class="row">
		
		<div class="col-sm-16">

			<div class="contact-form">
				<div class="row">
					<div class="col-sm-4">
						<div class="send-email">
							Send an<br>
							<span class="big">E-Mail</span>
							<div class="sep"><hr></div>
						</div>
					</div>

					<div class="col-sm-12">
						<?php echo do_shortcode('[gravityform id="1" name="Contact Form" title="false" description="false" ajax="false"]'); ?>
					</div>
				</div>
			</div>
		</div>
		
	</div>

</section>	