<section>

	<div class="row">
		<div class="col-sm-4">
			<div class="title">
				<p class="small">Our</p>
				<p class="big">Contact</p>
				<p class="medium">Details</p>
				<div class="sep"><hr></div>
			</div>
		</div>

		<div class="col-sm-12">
			<div class="copy">
				<div class="row">
					<div class="col-md-10 col-sm-9">
						<?php the_content(); ?>
					</div>
					<div class="col-md-5 col-md-offset-1 col-sm-7">
						<strong><?php the_field('company-name', 'option'); ?></strong><br />
						<?php the_field('address', 'option'); ?>

                    	<p>
                        t. <strong><?php the_field('phone', 'option'); ?></strong><br/>
                        e. <strong><a href="mailto: <?php echo antispambot(get_field('e-mail', 'option')); ?>"><?php the_field('e-mail', 'option'); ?></a></strong>
                    </p>

					</div>
				</div>	
			</div>
		</div>

	</div>

</section>