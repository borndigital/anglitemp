<section>

	<div class="row">
		<div class="col-sm-4">
			<div class="personnel-controls">
				<div class="row">
					<div class="col-xs-10 col-sm-16">
						Our<br>
						<span class="big">Personnel</span><br>
						<div class="sep"></div>
					</div>
					<div class="col-xs-6 col-sm-16">
						<div class="controls">
							<span class="personnel-prev"><i class="fa fa-chevron-left"></i></span>
							<span class="personnel-next"><i class="fa fa-chevron-right"></i></span>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-sm-12">
			<div class="personnel">
				<div id="personnel-slider" class="owl-carousel personnel-carousel">
 
    				<?php while ( have_rows('personnel') ) : the_row(); ?>
						
						<div class="person">
							<img src="<?php the_sub_field('image'); ?>">
							<div class="desc">
								<h3><?php the_sub_field('name'); ?></h3>
								<h4><?php the_sub_field('role'); ?></h4>
								<div class="sep"><hr></div>
								<div class="contact-details">
									<div class="row">
										
										<?php if(get_sub_field('e-mail')) { ?>
											<div class="col-md-8">
												<p>
													<a href="mailto:<?php the_sub_field('e-mail'); ?>"?>
														<i class="fa fa-envelope fa-lg"></i>Email
													</a>
												</p>
											</div>
										<?php } ?>

										<?php if(get_sub_field('linkedin')) { ?>
											<div class="col-md-8">
												<p>
													<a href="<?php the_sub_field('linkedin'); ?>" target="blank">
														<i class="fa fa-linkedin-square fa-lg"></i> Profile
													</a>
												</p>
											</div>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>

					<?php endwhile; ?>

				</div>

			</div>
		</div>

	</div>

</section>