<style>
    .pagination {
        display: block;
        text-align: center;
        padding: 10px 0;
    }
    .pagination a,
    .pagination span {
        margin-right: 5px;
        background-color: #FFF;
        padding: 15px 20px;
    }
    .pagination a:hover,
    .pagination span:hover,
    .pagination span.current {
        background-color: #EEE;
    }
</style>
<div class="pagination">
    <?php echo paginate_links(); ?>
</div>
