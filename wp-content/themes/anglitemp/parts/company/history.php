<?php
  $history = get_field('history');
?>

<?php if (!empty($history)): ?>
  <section>

  	<div class="row">
  		<div class="col-sm-4">
  			<div class="title history-title">
  				Our<br>
  				<span class="big">Sister company</span>
  				<div class="sep"><hr></div>
  			</div>
  		</div>

  		<div class="col-sm-12">
  			<div class="copy history-copy">
  				<?php echo $history; ?>
  			</div>
  		</div>

  	</div>

  </section>
<?php endif; ?>
