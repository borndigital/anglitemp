<section style="display:none">

	<div class="row">
		<div class="col-sm-4">
			<div class="title directors-title">
				Our<br>
				<span class="big">Directors</span>
				<div class="sep"><hr></div>
			</div>
		</div>

		<div class="col-sm-12">
			<div class="directors-copy">
				<div class="row">
					<div class="col-sm-6">
						<?php the_post_thumbnail(); ?>
					</div>

					<div class="col-sm-10">

						<?php the_field('profile_text'); ?>

						<a href="<?php the_field('gordons_linkedin'); ?>" target="blank" class="button">
							<i class="fa fa-linkedin-square fa-lg"></i> Gordon's Profile
						</a>

						<a href="<?php the_field('toms_linkedin'); ?>" target="blank" class="button">
							<i class="fa fa-linkedin-square fa-lg"></i> Tom's Profile
						</a>

					</div>
				</div>
			</div>
		</div>

	</div>

</section>