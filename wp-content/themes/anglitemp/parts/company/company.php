<section>

	<div class="row">
		<div class="col-sm-4">
			<div class="title company-title no-logo">
				Our<br>
				<span class="big">Company</span>
				<div class="sep"><hr></div>
				<p class="download">Download the <?php the_title(); ?> PDF datasheet.</p>
				<a href="<?php the_field('our_company_datasheet'); ?>" class="button">Download <i class="fa fa-chevron-down"></i></a>
			</div>
		</div>

		<div class="col-sm-12">
			<div class="copy company-copy">
				<?php the_field('company_text'); ?>
			</div>
		</div>

	</div>
</section>
