<div class="row">
	<div class="col-xs-8">
		<div class="row">
			<div class="col-sm-8">
				<div class="client">
					<div class="top">
						<?php 
						$client_id = get_field('client');
						$logo_src = get_post_meta($client_id->ID, 'logo', true);
						$logo = wpthumb( $logo_src, 'height=94' );
						$image = wp_get_attachment_image_src( $logo, 'height=47' );
						?>
						<img src="<?php echo $image[0]; ?>">
					</div>
					<div class="sep"><hr></div>
					<span class="type">Client</span>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="location">
					<div class="top">
						<i class="fa fa-map-marker fa-3x"></i> 
						<?php 
						$terms = get_the_terms($post->ID, 'countries');
						echo array_shift(array_values($terms))->name;
						?> 
					</div>
					<div class="sep"><hr></div>
					<span class="type">Location</span>
				</div>
			</div>
			<div class="col-sm-16">
				<div class="meta">
					<p><b>Application</b>: <?php the_field('application'); ?></p>
					<p><b>Spec</b>: <?php the_field('spec'); ?></p>
					<p><b>Insulation Product</b>: <?php the_field('insulation_product'); ?></p>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-8">
		<div class="row">
			<div class="col-sm-8">
				<div class="date">
					<div class="top">
						<?php
						// date is stored yyyymmdd
						$date = get_field('project_date');
						$month = __('September');
						$year = 2013;

						if ($date != false) {
							$date = strtotime($date);
							$month = date('M', $date);
							$year = date('Y', $date);
						}
						?>
						<?php echo $month; ?> <br>
						<span class="big">
							<?php echo $year; ?>              
						</span>
					</div>
				</div>
				<div class="sep"><hr></div>
				<span class="type">Date</span>
			</div>
			<div class="col-sm-8">
				<div class="quantity">
					<div class="top">
						<span class="big"><?php the_field('value'); ?></span><br>
						<?php the_field('measurement'); ?>
					</div>
				</div>
				<div class="sep"><hr></div>
				<span class="type"><?php the_field('stat_name'); ?></span>
			</div>
		</div>
	</div>
</div>
