<section>
		
	<div class="row">

		<div class="col-sm-4">

			<div class="search-results">
				Search<br>
				<span class="big">Results</span>
				<div class="sep"><hr></div>

				<form method="POST">
					<div class="select-style">
						<select name="type" class="search-by-type" id="type-search">
							<?php

							if (is_tax('type')) {
								$tax_slug = get_queried_object()->slug;
							}

							$product_types = get_terms('type');

							foreach ( $product_types as $type ) { ?>

								<option value="<?php bloginfo('url'); ?>/type/<?php echo $type->slug; ?>/" <?php selected( $tax_slug, $type->slug ); ?>><?php echo $type->name; ?></option>

							<?php } ?>

						</select>
					</div>
					
				</form>

			</div>

		</div>

		<div class="col-sm-12">

			<div class="results">

				<?php if(is_single()) { 
					the_post_thumbnail( 'width=710&height=230&crop=1');
				} ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<div class="result">

              <?php get_template_part('parts/projects/stats'); ?>

  						<?php if(is_single()) { ?>
	  						<div class="row">
	  							<div class="col-sm-16">
	  								<div class="content">
	  									<?php the_content(); ?>
	  								</div>
	  							</div>
	  						</div>
	  					<?php } ?>
					</div>

				<?php endwhile; ?>

			</div>

		</div>

	</div>

</section>