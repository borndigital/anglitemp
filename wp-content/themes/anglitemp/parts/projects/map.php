<section class="hidden-xs">
		
	<div class="row">

		<div class="col-md-4">

			<div class="browse-projects">
				Browse<br>
				<span class="big">Projects</span>
				<div class="sep"><hr></div>
			</div>

		</div>

		<div class="col-md-12">

			<div class="map-container">
				<h3>Select Projects by Country</h3>
				<div id="vmap" class="map"></div>
			</div>
		</div>

	</div>

</section>