<section>

	<div class="row">

		<div class="col-sm-4">

			<div class="search-results anglitemp">

				Latest<br>
				<span class="big">Projects</span>
				<div class="sep"><hr></div>

				<div id="projects-slider-name" class="owl-carousel">

					<?php
					$projects = get_element('project', -1);
					while($projects->have_posts()) : $projects->the_post(); ?>
						<div>
							<?php the_title(); ?>,
							<?php $terms = get_the_terms($post->ID, 'countries');
							echo array_shift(array_values($terms))->name; ?>
						</div>
					<?php endwhile; wp_reset_postdata(); ?>

				</div>


				<div class="controls">
					<span class="projects-prev"><i class="fa fa-chevron-left"></i></span>
					<span class="projects-next"><i class="fa fa-chevron-right"></i></span>
				</div>


				Search<br>
				<span class="big">Projects</span>
				<div class="sep"><hr></div>

				<form method="POST">
					<div class="select-style">
						<select name="type" class="search-by-type" id="type-search">
							<?php

							$product_types = get_terms('type');

							foreach ( $product_types as $type ) { ?>

								<option value="<?php bloginfo('url'); ?>/type/<?php echo $type->slug; ?>/" <?php selected( $tax_slug, $type->slug ); ?>><?php echo $type->name; ?></option>

							<?php } ?>

						</select>
					</div>

				</form>

			</div>

		</div>

		<div class="col-sm-12">

			<div class="results project-single-result">

				<div id="projects-slider" class="owl-carousel">

					<?php while ( $projects->have_posts() ) : $projects->the_post(); ?>

						<div>

							<?php the_post_thumbnail( 'width=710&height=230&crop=1'); ?>

							<div class="result">

                  <?php get_template_part('parts/projects/stats'); ?>

			  					<div class="row">
		  							<div class="col-sm-16">
		  								<div class="content">
		  									<?php the_content(); ?>
		  								</div>
		  							</div>
		  						</div>

							</div>

						</div>

					<?php endwhile; ?>

				</div>

			</div>

		</div>

	</div>

</section>
