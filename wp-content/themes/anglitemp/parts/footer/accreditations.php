<div class="col-sm-8 hidden-xs">
    <h3 class="accred-title">Membership &amp; Accreditations</h3>
    <div class="sep accred-hr"><hr></div>
    <div class="accreditations">
    	<div class="row">
    		<div class="col-md-5 col-md-16">
        		<div class="accred accred1">
              <img src="<?php bloginfo('template_url'); ?>/images/accred1.jpg" alt="Accreditation">
            </div>
        	</div>
        	<div class="col-md-6 col-md-16">
        		<div class="accred accred2">
              <img src="<?php bloginfo('template_url'); ?>/images/accred2.png" alt="Accreditation">
            </div>
        	</div>
        	<div class="col-md-5 col-md-16">
        		<div class="accred accred3">
              <img src="<?php bloginfo('template_url'); ?>/images/accred4.jpg" alt="Accreditation">
            </div>
        	</div>
        </div>
    </div>
</div>