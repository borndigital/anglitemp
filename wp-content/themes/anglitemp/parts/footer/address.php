<div class="col-sm-4">
    <h3>Address</h3>
    <div class="sep"><hr></div>
    <strong><?php the_field('company-name', 'option'); ?></strong><br />
    <?php the_field('address', 'option'); ?>

    <p>
        <?php the_field('phone', 'option'); ?><br/>
        <a href="mailto: <?php echo antispambot(get_field('e-mail', 'option')); ?>"><?php the_field('e-mail', 'option'); ?></a>
    </p>

</div>