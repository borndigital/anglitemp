<div class="comparisons">
	<div class="col-sm-12">
		<?php 

		while ( have_rows('product') ) : the_row(); ?>
			<div class="comparison comparison-2 bottom-row">
				<table class="table">
					<thead>
						<tr>
							<td><?php the_sub_field('name'); ?></td>
						</tr>
					</thead>

					<tbody>
						<?php while ( have_rows('stats') ) : the_row(); ?>
										
							<tr>
								<td><strong><?php the_sub_field('stat'); ?></strong></td>
								<td class="stat"><?php the_sub_field('value'); ?></td>
							</tr>

						<?php endwhile; ?>
					</tbody>

				</table>
			</div>
		<?php endwhile; ?>
	</div>
</div>