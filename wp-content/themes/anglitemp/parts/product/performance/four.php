<div class="col-sm-12">
	<div class="row">

		<div class="comparisons">

			<?php 
			$counter = 1;
			while ( have_rows('product') ) : the_row(); ?>
				
				<div class="col-sm-8">
					<div class="comparison comparison-<?php echo $counter; ?> <?php if ($counter > 2) { echo 'bottom-row'; } ?>">
						<table class="table">
							<thead>
								<tr>
									<td><?php the_sub_field('name'); ?></td>
								</tr>
							</thead>

							<tbody>

								<?php while ( have_rows('stats') ) : the_row(); ?>
									
									<tr>
										<td><strong><?php the_sub_field('stat'); ?></strong></td>
										<td class="stat"><?php the_sub_field('value'); ?></td>
									</tr>

								<?php endwhile; ?>

							</tbody>

						</table>
					</div>
				</div>

			<?php $counter++; endwhile; ?>

		</div>
	</div>
</div>