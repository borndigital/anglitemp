<section class="hidden-xs">
	<div class="row">

		<div class="col-sm-4">
			<div class="case-study-controls home-block-small">
				<div class="row">
					<div class="col-xs-8 col-sm-16">
						Case<br>
						<span class="big">Studies</span><br>
						<div class="sep"></div>

						<div id="projects-slider-name" class="owl-carousel">

							<?php
              $projects = ang_product_case_studies();

							while($projects->have_posts()) : $projects->the_post(); ?>
								<div>
									<?php the_title(); ?>,
									<?php $terms = get_the_terms($post->ID, 'countries');
									echo array_shift(array_values($terms))->name; ?>
								</div>
							<?php endwhile; wp_reset_postdata(); ?>

						</div>
					</div>
					<div class="col-xs-8 col-sm-16">
						<div class="controls">
							<span class="projects-prev"><i class="fa fa-chevron-left"></i></span>
							<span class="projects-next"><i class="fa fa-chevron-right"></i></span>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-sm-12">
			<div class="home-block-large home-block-projects">

				<div id="projects-slider" class="owl-carousel project-carousel">

					<?php

					$projects = get_element('project', -1);

					while($projects->have_posts()) : $projects->the_post(); ?>

  						<div class="row">

  							<div class="col-xs-8">

  								<div class="row">

  									<div class="col-sm-8">
										<div class="client">
											<div class="top">
												<?php
												$client_id = get_field('client');
												$logo_src = get_post_meta($client_id->ID, 'logo', true);
                    							$logo = wpthumb( $logo_src, 'height=94' );
												$image = wp_get_attachment_image_src( $logo, 'height=47' );
												?>
												<img src="<?php echo $image[0]; ?>">
											</div>
											<div><hr></div>
											<span class="type">Client</span>
										</div>
									</div>

									<div class="col-sm-8">
										<div class="location">
											<div class="top">
												<i class="fa fa-map-marker fa-3x"></i>
												<?php
												$terms = get_the_terms($post->ID, 'countries');
        										echo array_shift(array_values($terms))->name; ?>
											</div>
											<div><hr></div>
											<span class="type">Location</span>
										</div>
									</div>

									<div class="col-sm-16">
										<div class="meta">
											<p><b>Application</b>: <?php the_field('application'); ?></p>
											<p><b>Spec</b>: <?php the_field('spec'); ?></p>
											<p><b>Insulation Product</b>: <?php the_field('insulation_product'); ?></p>
										</div>
									</div>

								</div>

							</div>

							<div class="col-xs-8">

								<div class="row">
  									<div class="col-sm-8">
  										<div class="date">
											<div class="top">
												<?php the_time('F'); ?> <br>
												<span class="big"><?php the_time('Y'); ?></span>
											</div>
										</div>
										<div><hr></div>
										<span class="type">Date</span>

									</div>

									<div class="col-sm-8">
										<div class="quantity">
											<div class="top">
												<span class="big"><?php the_field('value'); ?></span><br>
												<?php the_field('measurement'); ?>
											</div>
										</div>
										<div class="sep"><hr></div>
										<span class="type"><?php the_field('stat_name'); ?></span>
									</div>

									<div class="col-sm-16">
										<br />
										<a href="<?php the_permalink(); ?>" class="button">Read More</a>
									</div>

								</div>

							</div>

  						</div>

					<?php endwhile; wp_reset_postdata(); ?>

				</div>

			</div>
		</div>

	</div>
</section>
