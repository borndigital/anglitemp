<section>
  <div class="row">
    <div class="col-sm-16">
      <form action="/" method="GET" class="product-search search">
        <div class="input-wrap">
          <input type="text" name="search_product" id="search" placeholder="search">
          <input type="submit" value="Search">
          <i class="fa fa-search"></i>
        </div>
        <p>Search and view a list of our Products</p>
      </form>
    </div>
  </div>
</section>
