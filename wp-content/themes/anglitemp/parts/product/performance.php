<?php if (get_field('product')) { ?>
	
	<section>

		<div class="row">
			<div class="col-sm-4">
				<div class="performance">
					<div class="header">
						<p class="small">Typical</p>
						<p class="big">Product</p>
						<p class="medium">Performance</p>
						<div class="sep"><hr></div>
					</div>
				</div>
			</div>

			<?php 

			if (1 == count(get_field('product'))) {
				get_template_part('parts/product/performance/one');
			} elseif (2 == count(get_field('product'))) {
				get_template_part('parts/product/performance/two');
			} elseif (3 == count(get_field('product'))) {
				get_template_part('parts/product/performance/three');
			} else {
				get_template_part('parts/product/performance/four');
			}

			?>
			
		</div>

	</section>

<?php } ?>