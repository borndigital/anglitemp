<?php if (get_field('testimonial')) {

	$testimonial = get_field('testimonial');

	?>
	<section class="hidden-xs">

		<div class="row">
			<div class="col-md-12 col-sm-11 col-xs-16">
				<div class="testimonial">
					<h3>Testimonial</h3>
					<hr>
					<blockquote>
						<?php the_field('quote', $testimonial); ?>
						<hr>
						<cite>
              <span class="nowrap"><strong><?php the_field('name', $testimonial); ?></strong>,</span>
              <span class="nowrap"><?php the_field('company', $testimonial); ?></span>
            </cite>
					</blockquote>
				</div>
			</div>

			<div class="col-md-4 col-sm-5">
				<div class="testimonial-image">
					<img src="<?php the_field('image', $testimonial); ?>">
				</div>
			</div>
		</div>

	</section>
<?php } ?>
