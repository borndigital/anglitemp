<section>

	<div class="row">
		<div class="col-sm-4">
			<div class="title">
				<div class="header">
					<p class="big"><?php the_field('first_line'); ?></p>
					<p class="small"><?php the_field('second_line'); ?></p>
				</div>
				<div class="sep"><hr></div>
				<p>Download the <?php the_title(); ?> PDF datasheet.</p>
				<a href="<?php the_field('datasheet'); ?>" target="blank" class="button">Download <i class="fa fa-chevron-down"></i></a>
			</div>
		</div>

		<div class="col-sm-8">
			<div class="copy">
				<?php the_content(); ?>
			</div>
		</div>

		<div class="col-sm-4 hidden-xs">
			<div class="image">
				<img src="<?php the_field('secondary_image'); ?>">
			</div>
		</div>
	</div>

</section>