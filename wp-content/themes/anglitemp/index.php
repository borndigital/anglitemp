<?php get_header(); ?>

<div class="container">

    <?php get_template_part('parts/news/before'); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		<section>

			<div class="row">

				<article>
					<div class="col-md-4 col-xs-5">
						<?php the_post_thumbnail( 'width=230&height=230&crop=1', array('class' => 'featured-image')); ?>
					</div>

					<div class="col-md-12 col-xs-11">

						<div class="overview">
							<p class="date"><?php the_date(); ?></p>
							<h2><?php the_title(); ?></h2>
							<div class="sep"><hr></div>
							<p><?php echo wp_trim_words(get_the_excerpt(), 25); ?></p>
							<a href="<?php the_permalink(); ?>" class="button">Read More</a>
						</div>

					</div>
				</article>

			</div>

		</section>

	<?php endwhile;  ?>

  <?php get_template_part( 'parts/pagination' ); ?>

</div>

<?php get_footer(); ?>
