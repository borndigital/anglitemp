<?php
/*
Template Name: Contact
*/
get_header(); ?>

<div class="container">

	<?php get_template_part('parts/contact/contact-details'); ?>

	<?php get_template_part('parts/contact/personnel'); ?>

	<?php get_template_part('parts/contact/contact-form'); ?>

</div>

<?php get_footer(); ?>
