<?php 
/*
Template Name: Our Company
*/
get_header(); ?>

<div class="container">
	
	<?php get_template_part('parts/company/company'); ?>

	<?php get_template_part('parts/company/directors'); ?>

	<?php get_template_part('parts/company/history'); ?>

</div>

<?php get_footer(); ?>