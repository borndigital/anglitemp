<?php
/*
Plugin Name: Anglitemp/BNT Common
Description: Core functionality of Anglitemp
Author: Steven Jones
Author URI: http://www.born-digital.co.uk
Version: 1.0
*/

/*
|--------------------------------------------------------------------------
| CONSTANTS
|--------------------------------------------------------------------------
*/

// Plugin folder url
if(!defined('ANG_PLUGIN_URL')) {
	define('ANG_PLUGIN_URL', plugin_dir_url( __FILE__ ));
}
// Plugin folder path
if(!defined('ANG_PLUGIN_DIR')) {
	define('ANG_PLUGIN_DIR', plugin_dir_path( __FILE__ ));
}
// Plugin root file
if(!defined('ANG_PLUGIN_FILE')) {
	define('ANG_PLUGIN_FILE', __FILE__);
}

/*
|--------------------------------------------------------------------------
| COMMON SCRIPTS
|--------------------------------------------------------------------------
*/
require_once dirname( __FILE__ ) . '/includes/clients.php';
require_once dirname( __FILE__ ) . '/includes/page-archives.php';
require_once dirname( __FILE__ ) . '/includes/products.php';
require_once dirname( __FILE__ ) . '/includes/projects.php';
require_once dirname( __FILE__ ) . '/includes/options.php';
require_once dirname( __FILE__ ) . '/includes/testimonials.php';
require_once dirname( __FILE__ ) . '/includes/setup.php';

/* Load plugin depending on the constant (set this in wp.config) */
if ('BNT' == ANG_VER) {
	require_once dirname( __FILE__ ) . '/bnt/products.php';
  require_once dirname( __FILE__ ) . '/bnt/branches.php';
  require_once dirname( __FILE__ ) . '/bnt/price-list-products.php';
  require_once dirname( __FILE__ ) . '/bnt/supplier-testimonials.php';
} else {

}