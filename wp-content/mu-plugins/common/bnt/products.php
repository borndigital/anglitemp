<?php

// Register markets

function register_market_taxonomy() {
	$labels = array(
		'name'              => 'Markets',
		'singular_name'     => 'Market',
		'search_items'      => 'Search Markets',
		'all_items'         => 'All Markets',
		'parent_item'       => 'Parent Market',
		'parent_item_colon' => 'Parent Market:',
		'edit_item'         => 'Edit Market',
		'update_item'       => 'Update Market',
		'add_new_item'      => 'Add New Market',
		'new_item_name'     => 'New Market Name',
		'menu_name'         => 'Markets',
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
	);

	register_taxonomy( 'markets', array( 'product' ), $args );
}
add_action( 'init', 'register_market_taxonomy', 0 );

function bnt_product_acf_fields($fields) {
  return array(
      array (
          'key' => 'field_53a2d9b30cfc9',
          'label' => 'Testimonial',
          'name' => 'testimonial',
          'type' => 'post_object',
          'post_type' => array (
              0 => 'testimonial',
          ),
          'taxonomy' => array (
              0 => 'all',
          ),
          'allow_null' => 1,
          'multiple' => 0,
      ),
      array (
        'key' => 'field_5490313a275b0',
        'label' => 'Related prices',
        'name' => 'related_prices',
        'type' => 'relationship',
        'return_format' => 'object',
        'post_type' => array (
          0 => 'price_list_product',
        ),
        'taxonomy' => array (
          0 => 'all',
        ),
        'filters' => array (
          0 => 'search',
          1 => 'post_type',
        ),
        'result_elements' => array (
          0 => 'post_type',
          1 => 'post_title',
        ),
        'max' => '',
      ),
      array (
        'key' => 'field_5490313a275b2',
        'label' => 'Supplier testimonials',
        'name' => 'supplier_testimonials',
        'type' => 'post_object',
        'post_type' => array (
            0 => 'supplier_testimonial',
        ),
        'taxonomy' => array (
            0 => 'all',
        ),
        'allow_null' => 1,
        'multiple' => 0,
      ),
      array (
        'key' => 'field_53a411f92cfa2',
        'label' => 'Category',
        'name' => 'category',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
  );
}
// add_filter('product_acf_fields', 'bnt_product_acf_fields');
