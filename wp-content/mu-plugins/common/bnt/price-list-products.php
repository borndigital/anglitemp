<?php

function bnt_register_price_list_products() {
    $labels = array(
        'name' => 'Pricelist',
        'singular_name' => 'Pricelist',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Pricelist',
        'edit_item' => 'Edit Pricelist',
        'new_item' => 'New Pricelist',
        'all_items' => 'All Pricelists',
        'view_item' => 'View Pricelists',
        'search_items' => 'Search Pricelists',
        'not_found' =>  'No Pricelists found',
        'not_found_in_trash' => 'No Pricelists found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => 'Price list'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'capability_type' => 'post',
        'has_archive' => false, 
        'hierarchical' => true,
        'menu_position' => 2,
        'menu_icon' => 'dashicons-cart',
        'supports' => array('title')
    ); 

    register_post_type( 'price_list_product', $args );
  
}
add_action('init', 'bnt_register_price_list_products');

function bnt_register_price_list_meta() {
  if(function_exists("register_field_group"))
  {
    register_field_group(array (
      'id' => 'acf_product-details',
      'title' => 'Pricelist details',
      'fields' => array (
        array (
          'key' => 'field_5489af2cf4fa6',
          'label' => 'Image (228px x 98px)',
          'name' => 'image',
          'type' => 'image',
          'save_format' => 'url',
          'preview_size' => 'full',
          'library' => 'all',
        ),
        array (
          'key' => 'field_5489af4df4fa7',
          'label' => 'Brand website',
          'name' => 'brand_website',
          'type' => 'text',
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'formatting' => 'html',
          'maxlength' => '',
        ),
        array (
          'key' => 'field_2489af4df4fa7',
          'label' => 'Update text',
          'name' => 'update_text',
          'type' => 'text',
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'formatting' => 'html',
          'maxlength' => '',
        ),
        array (
          'key' => 'field_5489af5df4fa8',
          'label' => 'Brand logo (123px x 39px)',
          'name' => 'brand_logo',
          'type' => 'image',
          'save_format' => 'url',
          'preview_size' => 'full',
          'library' => 'all',
        ),
        array (
          'key' => 'field_5489af5df4fa3',
          'label' => 'Brand logo (142px x 142px)',
          'name' => 'brand_logo_large',
          'type' => 'image',
          'save_format' => 'url',
          'preview_size' => 'full',
          'library' => 'all',
        ),
        array (
          'key' => 'field_5489afc7f4fa9',
          'label' => 'Price list',
          'name' => 'attachment',
          'type' => 'file',
          'save_format' => 'url',
          'library' => 'all',
        ),
        array (
          'key' => 'field_4482afc7f7fa9',
          'label' => 'Datasheet',
          'name' => 'datasheet',
          'type' => 'file',
          'save_format' => 'url',
          'library' => 'all',
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'post_type',
            'operator' => '==',
            'value' => 'price_list_product',
            'order_no' => 0,
            'group_no' => 0,
          ),
        ),
      ),
      'options' => array (
        'position' => 'normal',
        'layout' => 'default',
        'hide_on_screen' => array (
        ),
      ),
      'menu_order' => 0,
    ));
  }

}
add_action('init', 'bnt_register_price_list_meta');

function get_recently_updated_price_list_products($since) {
  return new WP_Query(array(
    'post_status' => 'publish',
    'post_type'   => 'price_list_product',
    'showposts'   => -1,
    'date_query' => array(
      array(
        'after' => $since,
      ),
    ),
  ));
}

/**
 * Get a list of products, sorted alphabetically
 */
function get_price_list($search = '') {

  $args = array(
    'post_status' => 'publish',
    'post_type'   => 'price_list_product',
    'showposts'   => -1,
    'orderby' => 'title',
    'order' => 'ASC',
  );

  if ($search !== '') {
    add_filter( 'posts_where', 'title_filter', 10, 2 );
    $args['search'] = $search;
  }

  $price_list_products = new WP_Query($args);

  if ($search !== '') {
    remove_filter( 'posts_where', 'title_filter', 10, 2 );
  }

  return $price_list_products;
}

/**
 * JS function to handle calls to the 
 */
function bnt_api_search_price_list() {
  hm_add_rewrite_rule(array(
    'regex' => '^api/price_list/?$',
    'query' => '',
    'request_callback' => function( WP $wp ) {

      $search = isset($_GET['search']) ? $_GET['search'] : '';
      $products = get_price_list($_GET['search']);

      // render products list
      ob_start();

      while ($products->have_posts()) : $products->the_post();
        get_template_part('parts/prices/item');
      endwhile; wp_reset_postdata();

      $html = ob_get_contents();
      ob_end_clean();

      // render response
      header('Content-type: application/json');
      echo json_encode(array(
        'found_posts' => $products->found_posts,
        'html' => $html,
      ));

      exit;
    },
  ));
}
add_action('init', 'bnt_api_search_price_list');
