<?php

/* Register Branches Post Type */
function bnt_register_branches() {
    $labels = array(
        'name' => 'Branches',
        'singular_name' => 'Branch',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Branch',
        'edit_item' => 'Edit Branch',
        'new_item' => 'New Branch',
        'all_items' => 'All Branches',
        'view_item' => 'View Branches',
        'search_items' => 'Search Branches',
        'not_found' =>  'No Branches found',
        'not_found_in_trash' => 'No Branches found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => 'Branches'
    );

    $labels = apply_filters('bnt_branch_labels', $labels);

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'capability_type' => 'post',
        'has_archive' => false, 
        'hierarchical' => true,
        'menu_position' => 2,
        'menu_icon' => 'dashicons-location',
        'supports' => array('title')
    ); 

    register_post_type( 'branches', $args );
  
}
add_action('init', 'bnt_register_branches');

function bnt_register_branch_meta() {
    if(function_exists("register_field_group"))
    {
      register_field_group(array (
        'id' => 'acf_branch-details',
        'title' => 'Branch details',
        'fields' => array (
          array (
            'key' => 'field_5489927d498c3',
            'label' => 'Phone number',
            'name' => 'phone_number',
            'type' => 'text',
            'required' => 1,
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'formatting' => 'html',
            'maxlength' => '',
          ),
          array (
            'key' => 'field_5489927d498c4',
            'label' => 'Email',
            'name' => 'email',
            'type' => 'text',
            'required' => 1,
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'formatting' => 'html',
            'maxlength' => '',
          ),
          array (
            'key' => 'field_54f6d8fhw453r',
            'label' => 'Image',
            'name' => 'image',
            'type' => 'image',
            'column_width' => '',
            'save_format' => 'url',
            'preview_size' => 'medium',
            'library' => 'all',
          ),
          array (
            'key' => 'field_548b1d589ee8a',
            'label' => 'Opening hours',
            'name' => 'opening_hours',
            'type' => 'repeater',
            'sub_fields' => array (
              array (
                'key' => 'field_548b1e58521df',
                'label' => 'Days',
                'name' => 'days',
                'type' => 'text',
                'column_width' => '',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'html',
                'maxlength' => '',
              ),
              array (
                'key' => 'field_548b1e68521e0',
                'label' => 'Times',
                'name' => 'times',
                'type' => 'text',
                'column_width' => '',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'html',
                'maxlength' => '',
              ),
            ),
            'row_min' => '',
            'row_limit' => '',
            'layout' => 'table',
            'button_label' => 'Add Row',
          ),
          array (
            'key' => 'field_53a2e9c5e976d',
            'label' => 'Personnel',
            'name' => 'personnel',
            'type' => 'repeater',
            'sub_fields' => array (
              array (
                'key' => 'field_53a2e9cde976e',
                'label' => 'Image',
                'name' => 'image',
                'type' => 'image',
                'column_width' => '',
                'save_format' => 'url',
                'preview_size' => 'medium',
                'library' => 'all',
              ),
              array (
                'key' => 'field_53a2e9d7e976f',
                'label' => 'Name',
                'name' => 'name',
                'type' => 'text',
                'column_width' => '',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'none',
                'maxlength' => '',
              ),
              array (
                'key' => 'field_53a2e9dde9770',
                'label' => 'Role',
                'name' => 'role',
                'type' => 'text',
                'column_width' => '',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'none',
                'maxlength' => '',
              ),
              array (
                'key' => 'field_55acc2412398e',
                'label' => 'Telephone',
                'name' => 'telephone',
                'type' => 'text',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'html',
                'maxlength' => '',
              ),
              array (
                'key' => 'field_53a2e9f9e9772',
                'label' => 'E-Mail',
                'name' => 'e-mail',
                'type' => 'email',
                'column_width' => '',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
              ),
              array (
                'key' => 'field_53a2e9e6e9771',
                'label' => 'LinkedIn',
                'name' => 'linkedin',
                'type' => 'text',
                'column_width' => '',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'none',
                'maxlength' => '',
              ),
            ),
            'row_min' => '',
            'row_limit' => '',
            'layout' => 'row',
            'button_label' => 'Add Person',
          ),
        ),
        'location' => array (
          array (
            array (
              'param' => 'post_type',
              'operator' => '==',
              'value' => 'branches',
              'order_no' => 0,
              'group_no' => 0,
            ),
          ),
          array (
            array (
              'param' => 'page_template',
              'operator' => '==',
              'value' => 'templates/contact.php',
              'order_no' => 0,
              'group_no' => 1,
            ),
          ),
        ),
        'options' => array (
          'position' => 'normal',
          'layout' => 'no_box',
          'hide_on_screen' => array (
          ),
        ),
        'menu_order' => 0,
      ));
    }

}
add_action('init', 'bnt_register_branch_meta');

function bnt_get_branches() {
 return new WP_Query(array(
    'post_status' => 'publish',
    'post_type'   => 'branches',
    'showposts'   => -1,
    'orderby'     => 'menu_order date',
    'order'       => 'ASC',
  ));
}

function bnt_get_contact_page() {
  $posts = get_posts(array(
    'post_type' => 'page',
    'meta_key' => '_wp_page_template',
    'meta_value' => 'templates/contact.php'
  ));
  return $posts[0];
}

/**
 * JS function to handle calls to the 
 */
function bnt_api_get_branch() {
  hm_add_rewrite_rule(array(
    'regex' => '^api/branch/?$',
    'query' => '',
    'request_callback' => function( WP $wp ) {
      $id = $_GET['id'];
      $post = $id == 0 ? bnt_get_contact_page() : get_post($id);

      // render response
      header('Content-type: application/json');
      echo json_encode(array(
        'title' => $post->post_title,
        'telephone' => get_field('phone_number', $post->ID),
      ));

      exit;
    },
  ));
}
add_action('init', 'bnt_api_get_branch');
