<?php

/* Register Projects Post Type */
function ang_register_projects() {
	
	$labels = array(
		'name' => 'Projects',
		'singular_name' => 'Project',
		'add_new' => 'Add New',
		'add_new_item' => 'Add New Project',
		'edit_item' => 'Edit Project',
		'new_item' => 'New Project',
		'all_items' => 'All Projects',
		'view_item' => 'View Projects',
		'search_items' => 'Search Projects',
		'not_found' =>  'No Projects found',
		'not_found_in_trash' => 'No Projects found in Trash', 
		'parent_item_colon' => '',
		'menu_name' => 'Projects'
  	);

  	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'query_var' => true,
		'capability_type' => 'post',
		'has_archive' => true, 
		'hierarchical' => true,
		'rewrite'   => array( 'slug' => 'projects' ),
		'menu_position' => 2,
		'menu_icon' => 'dashicons-format-chat',
		'supports' => array('title', 'editor', 'thumbnail')
  	); 

  	register_post_type( 'project', $args );
  
}
add_action('init', 'ang_register_projects');

function ang_register_countries() {

	$labels = array(
		'name'			  => 'Countries',
		'singular_name'	 => 'Country',
		'search_items'	  => 'Search Countries',
		'all_items'		 => 'All Countries',
		'parent_item'	   => 'Parent Country',
		'parent_item_colon' => 'Parent Country',
		'edit_item'		 => 'Edit Country',
		'update_item'	   => 'Update Country',
		'add_new_item'	  => 'Add New Country',
		'new_item_name'	 => 'New Country Name',
		'menu_name'		 => 'Countries',
	);

	$args = array(
		'hierarchical'	  => true,
		'labels'			=> $labels,
		'show_ui'		   => true,
		'show_admin_column' => true,
		'query_var'		 => true,
		'rewrite'		   => array( 'slug' => 'countries' ),
	);

	register_taxonomy( 'countries', array( 'project' ), $args );

}
add_action('init', 'ang_register_countries');

function remove_artist_meta() {
	remove_meta_box( 'countriesdiv', 'project', 'side' );
}

add_action( 'admin_menu' , 'remove_artist_meta' );

function ang_register_types() {

	$labels = array(
		'name'			  => 'Types',
		'singular_name'	 => 'Type',
		'search_items'	  => 'Search Types',
		'all_items'		 => 'All Types',
		'parent_item'	   => 'Parent Type',
		'parent_item_colon' => 'Parent Type',
		'edit_item'		 => 'Edit Type',
		'update_item'	   => 'Update Type',
		'add_new_item'	  => 'Add New Type',
		'new_item_name'	 => 'New Type Name',
		'menu_name'		 => 'Type',
	);

	$args = array(
		'hierarchical'	  => true,
		'labels'			=> $labels,
		'show_ui'		   => false,
		'show_admin_column' => true,
		'query_var'		 => true,
		'rewrite'		   => array( 'slug' => 'type' ),
	);

	register_taxonomy( 'type', array( 'project' ), $args );

}
add_action('init', 'ang_register_types');

function get_countries() {

	$countries = get_terms("countries");
	$countries_array = array();

	foreach ($countries as $country) {
		$countries_array[] = $country->slug;
	}

	return $countries_array;
}

function update_product_to_type($post_id) {

	if ( 'project' != $_POST['post_type'] ) {
		return;
	}

	// Have to use the ACF field ID
	$product_id = $_POST['fields']['field_53a2de8cbcfeb'];

	$product = get_post($product_id);

	$term = term_exists($product->post_title, 'type');
	
	if ($term == 0 || $term == null) {
		wp_insert_term($product->post_title, 'type');
	}

	wp_set_object_terms( $post_id, $product->post_title, 'type' );

}
add_action('save_post', 'update_product_to_type');

function ang_register_project_meta() {
	if(function_exists("register_field_group"))
	{
		$project_fields = array (
			array (
				'key' => 'field_53a2de8cbcfeb',
				'label' => 'Type',
				'name' => 'type',
				'type' => 'post_object',
				'post_type' => array (
					0 => 'product',
				),
				'taxonomy' => array (
					0 => 'all',
				),
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_53a2e4fb1111a',
				'label' => 'Country',
				'name' => 'country',
				'type' => 'taxonomy',
				'taxonomy' => 'countries',
				'field_type' => 'select',
				'allow_null' => 0,
				'load_save_terms' => 1,
				'return_format' => 'id',
				'multiple' => 0,
			),
		);

		$project_fields = apply_filters('project_acf_fields', $project_fields);

		register_field_group(array (
			'id' => 'acf_project-categories',
			'title' => 'Project Categories',
			'fields' => $project_fields,
      'location' => array (
          array (
              array (
                  'param' => 'post_type',
                  'operator' => '==',
                  'value' => 'project',
                  'order_no' => 0,
                  'group_no' => 0,
              ),
          ),
      ),
      'options' => array (
          'position' => 'side',
          'layout' => 'default',
          'hide_on_screen' => array (
          ),
      ),
      'menu_order' => 0,
		));

    $project_information_acf_fields = array (
      array (
        'key' => 'field_55acbaf56adf4',
        'label' => 'Project date',
        'name' => 'project_date',
        'type' => 'date_picker',
        'date_format' => 'yymmdd',
        'display_format' => 'dd/mm/yy',
        'first_day' => 1,
      ),
      array (
        'key' => 'field_53a411f92cfa5',
        'label' => 'Application',
        'name' => 'application',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_53a412002cfa6',
        'label' => 'Spec',
        'name' => 'spec',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_53a412062cfa7',
        'label' => 'Insulation Product',
        'name' => 'insulation_product',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
    );
    $project_information_acf_fields = apply_filters('project_information_acf_fields', $project_information_acf_fields);

		register_field_group(array (
			'id' => 'acf_project-information-2',
			'title' => 'Project Information',
			'fields' => $project_information_acf_fields,
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'project',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'normal',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));
	}

	if(function_exists("register_field_group"))
	{
		register_field_group(array (
			'id' => 'acf_statistic',
			'title' => 'Statistic',
			'fields' => array (
				array (
					'key' => 'field_53aac529eabb2',
					'label' => 'Stat Name',
					'name' => 'stat_name',
					'type' => 'text',
					'instructions' => 'e.g. Quantity',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'none',
					'maxlength' => '',
				),
				array (
					'key' => 'field_53aac538eabb3',
					'label' => 'Value',
					'name' => 'value',
					'type' => 'text',
					'instructions' => 'e.g. 24',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'none',
					'maxlength' => '',
				),
				array (
					'key' => 'field_53aac53eeabb4',
					'label' => 'Measurement',
					'name' => 'measurement',
					'type' => 'text',
					'instructions' => 'e.g. units',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'none',
					'maxlength' => '',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'project',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'normal',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 4,
		));
	}
}
add_action('init', 'ang_register_project_meta');

/**
 * Register client meta information for projects
 */
function ang_register_project_client_meta() {
	if (!function_exists("register_field_group")) return;

	register_field_group(array (
		'id' => 'acf_client',
		'title' => 'Client',
		'fields' => array (
			array (
				'key' => 'field_53a4103eecd59',
				'label' => 'Client',
				'name' => 'client',
				'type' => 'post_object',
				'post_type' => array (
					0 => 'clients',
				),
				'taxonomy' => array (
					0 => 'all',
				),
				'allow_null' => 0,
				'multiple' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'project',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'side',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
add_action('init', 'ang_register_project_client_meta', 200);

