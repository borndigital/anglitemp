<?php

/* Register Clients Post Type */
function ang_register_clients() {
    
    $labels = array(
        'name' => 'Clients',
        'singular_name' => 'Client',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Client',
        'edit_item' => 'Edit Client',
        'new_item' => 'New Client',
        'all_items' => 'All Clients',
        'view_item' => 'View Clients',
        'search_items' => 'Search Clients',
        'not_found' =>  'No Clients found',
        'not_found_in_trash' => 'No Clients found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => 'Clients'
    );

    $labels = apply_filters('ang_client_labels', $labels);

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'capability_type' => 'post',
        'has_archive' => false, 
        'hierarchical' => true,
        'menu_position' => 2,
        'menu_icon' => 'dashicons-businessman',
        'supports' => array('title')
    ); 

    register_post_type( 'clients', $args );
  
}
add_action('init', 'ang_register_clients');

function ang_register_client_meta() {
    if(function_exists("register_field_group"))
    {
        register_field_group(array (
            'id' => 'acf_logo',
            'title' => 'Logo',
            'fields' => array (
                array (
                    'key' => 'field_53a83520be4ce',
                    'label' => 'Logo',
                    'name' => 'logo',
                    'type' => 'image',
                    'save_format' => 'url',
                    'preview_size' => 'large',
                    'library' => 'all',
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'clients',
                        'order_no' => 0,
                        'group_no' => 0,
                    ),
                ),
            ),
            'options' => array (
                'position' => 'normal',
                'layout' => 'default',
                'hide_on_screen' => array (
                ),
            ),
            'menu_order' => 0,
        ));

        register_field_group(array (
            'id' => 'acf_client-2',
            'title' => 'Client',
            'fields' => array (
                array (
                    'key' => 'field_53b2be3e08b85',
                    'label' => 'Show on homepage?',
                    'name' => 'show_on_homepage',
                    'type' => 'true_false',
                    'message' => '',
                    'default_value' => 0,
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'clients',
                        'order_no' => 0,
                        'group_no' => 0,
                    ),
                ),
            ),
            'options' => array (
                'position' => 'normal',
                'layout' => 'default',
                'hide_on_screen' => array (
                ),
            ),
            'menu_order' => 5,
        ));
    }
}
add_action('init', 'ang_register_client_meta');

function get_clients() {

    $args = array(
      'post_status' => 'publish',
      'post_type'   => 'clients',
      'showposts'   => -1,
      'orderby'     => 'rand',
      'meta_query' => array(
        array(
            'key' => 'show_on_homepage',
            'value' => '1',
            'compare' => '=',
            'type' => 'numeric',
        )
    )
 
   );

   $clients = new WP_Query($args);

   return $clients;

}

function api_get_clients() {

    hm_add_rewrite_rule( 

        array(
            'regex' => '^api/clients/?$',
            'query' => '',
            'request_callback' => function( WP $wp ) {

                $body = file_get_contents("php://input");

                header('Content-type: application/json');

                $items['items'] = array();

                $clients = get_clients();

                while($clients->have_posts()) : $clients->the_post();

                    $logo = get_field('logo', get_the_ID());
                    $image = wpthumb( $logo, 'height=144' );

                    $item = array('img' => $image, 'alt' => get_the_title() );
                    array_push($items['items'], $item);

                endwhile; wp_reset_postdata();

                echo json_encode( $items, JSON_FORCE_OBJECT );

                exit;
            }
        ) 
    );
}
add_action('init', 'api_get_clients');
