<?php

/* Register Product Post Type */
function ang_register_product() {

	$labels = array(
    	'name' => 'Products',
    	'singular_name' => 'Product',
    	'add_new' => 'Add New',
    	'add_new_item' => 'Add New Product',
    	'edit_item' => 'Edit Product',
    	'new_item' => 'New Product',
    	'all_items' => 'All Products',
    	'view_item' => 'View Products',
    	'search_items' => 'Search Products',
    	'not_found' =>  'No Products found',
    	'not_found_in_trash' => 'No Product found in Trash',
    	'parent_item_colon' => '',
    	'menu_name' => 'Products'
  	);

  	$args = array(
    	'labels' => $labels,
    	'public' => true,
    	'publicly_queryable' => true,
    	'show_ui' => true,
        'show_in_menu' => true,
    	'query_var' => true,
    	'capability_type' => 'post',
    	'has_archive' => true,
    	'hierarchical' => true,
    	'menu_position' => 2,
        'rewrite'   => array( 'slug' => 'products' ),
    	'menu_icon' => 'dashicons-admin-tools',
    	'supports' => array('title', 'editor', 'excerpt', 'thumbnail')
  	);

  	register_post_type( 'product', $args );

}
add_action('init', 'ang_register_product');


function ang_register_product_meta() {
    if(function_exists("register_field_group"))
    {
        register_field_group(array (
            'id' => 'acf_datasheets',
            'title' => 'Datasheet',
            'fields' => array (
                array (
                    'key' => 'field_537bhabb5fd24',
                    'label' => 'Datasheet PDF',
                    'name' => 'datasheet',
                    'type' => 'file',
                    'save_format' => 'url',
                    'library' => 'all',
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'product',
                        'order_no' => 0,
                        'group_no' => 0,
                    ),
                ),
            ),
            'options' => array (
                'position' => 'side',
                'layout' => 'default',
                'hide_on_screen' => array (
                ),
            ),
            'menu_order' => 0,
        ));

        register_field_group(array (
            'id' => 'acf_performance',
            'title' => 'Performance',
            'fields' => array (
                array (
                    'key' => 'field_53b67ae722dd0',
                    'label' => 'Product',
                    'name' => 'product',
                    'type' => 'repeater',
                    'sub_fields' => array (
                        array (
                            'key' => 'field_53b6843b22dd1',
                            'label' => 'Name',
                            'name' => 'name',
                            'type' => 'text',
                            'column_width' => '',
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'formatting' => 'none',
                            'maxlength' => '',
                        ),
                        array (
                            'key' => 'field_53b6844022dd2',
                            'label' => 'Stats',
                            'name' => 'stats',
                            'type' => 'repeater',
                            'column_width' => '',
                            'sub_fields' => array (
                                array (
                                    'key' => 'field_53b6844b22dd3',
                                    'label' => 'Stat',
                                    'name' => 'stat',
                                    'type' => 'text',
                                    'column_width' => '',
                                    'default_value' => '',
                                    'placeholder' => '',
                                    'prepend' => '',
                                    'append' => '',
                                    'formatting' => 'none',
                                    'maxlength' => '',
                                ),
                                array (
                                    'key' => 'field_53b6845322dd4',
                                    'label' => 'Value',
                                    'name' => 'value',
                                    'type' => 'text',
                                    'column_width' => '',
                                    'default_value' => '',
                                    'placeholder' => '',
                                    'prepend' => '',
                                    'append' => '',
                                    'formatting' => 'none',
                                    'maxlength' => '',
                                ),
                            ),
                            'row_min' => '',
                            'row_limit' => '',
                            'layout' => 'table',
                            'button_label' => 'Add Stat',
                        ),
                    ),
                    'row_min' => '',
                    'row_limit' => '4',
                    'layout' => 'table',
                    'button_label' => 'Add Product',
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'product',
                        'order_no' => 0,
                        'group_no' => 0,
                    ),
                ),
            ),
            'options' => array (
                'position' => 'normal',
                'layout' => 'default',
                'hide_on_screen' => array (
                ),
            ),
            'menu_order' => 0,
        ));

        // register product advanced custom fields
        $product_acf_fields = array(
            array (
                'key' => 'field_53a2d9502a71e',
                'label' => 'Case Studies',
                'name' => 'case_studies',
                'type' => 'relationship',
                'return_format' => 'object',
                'post_type' => array (
                    0 => 'project',
                ),
                'taxonomy' => array (
                    0 => 'all',
                ),
                'filters' => array (
                    0 => 'search',
                ),
                'result_elements' => array (
                    0 => 'post_type',
                    1 => 'post_title',
                ),
                'max' => '',
            ),
            array (
                'key' => 'field_53a2d9b30cfc9',
                'label' => 'Testimonial',
                'name' => 'testimonial',
                'type' => 'post_object',
                'post_type' => array (
                    0 => 'testimonial',
                ),
                'taxonomy' => array (
                    0 => 'all',
                ),
                'allow_null' => 1,
                'multiple' => 0,
            ),
        );
        $product_acf_fields = apply_filters('product_acf_fields', $product_acf_fields);
        register_field_group(array(
          'id' => 'acf_products',
          'title' => 'Products',
          'fields' => $product_acf_fields,
          'location' => array (
              array (
                  array (
                      'param' => 'post_type',
                      'operator' => '==',
                      'value' => 'product',
                      'order_no' => 0,
                      'group_no' => 0,
                  ),
              ),
          ),
          'options' => array (
              'position' => 'normal',
              'layout' => 'default',
              'hide_on_screen' => array (
              ),
          ),
          'menu_order' => 0,
        ));

        // register acf secondary images
        register_field_group(array (
            'id' => 'acf_secondary-image',
            'title' => 'Secondary Image',
            'fields' => array (
                array (
                    'key' => 'field_53a2dbc5dec5c',
                    'label' => '',
                    'name' => 'secondary_image',
                    'type' => 'image',
                    'save_format' => 'url',
                    'preview_size' => 'medium',
                    'library' => 'all',
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'product',
                        'order_no' => 0,
                        'group_no' => 0,
                    ),
                ),
            ),
            'options' => array (
                'position' => 'side',
                'layout' => 'default',
                'hide_on_screen' => array (
                ),
            ),
            'menu_order' => 0,
        ));

    }
}
add_action('init', 'ang_register_product_meta');

function title_filter( $where, &$wp_query )
{
  global $wpdb;
  if ( $search_term = $wp_query->get( 'search' ) ) {
    $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql( like_escape( $search_term ) ) . '%\'';
  }
  return $where;
}

function ang_get_products($search = '', $limit = 7) {
  $args = array(
    'post_status' => 'publish',
    'post_type'   => 'product',
    'showposts'   => $limit,
    'orderby'     => 'menu_order date',
    'order'       => 'ASC'
  );

  if ($search !== '') {
    add_filter( 'posts_where', 'title_filter', 10, 2 );
    $args['search'] = $search;
  }

  $products = new WP_Query($args);

  if ($search !== '') {
    remove_filter( 'posts_where', 'title_filter', 10, 2 );
  }

  return $products;
}

function api_search_products() {
  hm_add_rewrite_rule(array(
    'regex' => '^api/products/?$',
    'query' => '',
    'request_callback' => function( WP $wp ) {

      $args = array(
        'post_status' => 'publish',
        'post_type'   => 'product',
        'showposts'   => 8,
        'orderby'     => 'title',
        'order'       => 'ASC',
      );

      if (isset($_GET['search']) && $_GET['search'] != '') {
        $args['search'] = $_GET['search'];
      }

      // search for products
      add_filter( 'posts_where', 'title_filter', 10, 2 );
      $products = new WP_Query($args);
      remove_filter( 'posts_where', 'title_filter', 10, 2 );

      // render products list
      ob_start();

      while ($products->have_posts()) : $products->the_post();
        get_template_part('parts/product/item');
      endwhile; wp_reset_postdata();

      $html = ob_get_contents();
      ob_end_clean();

      // render response
      header('Content-type: application/json');
      echo json_encode(array(
        'found_posts' => $products->found_posts,
        'html' => $html,
      ));

      exit;
    },
  ));
}
add_action('init', 'api_search_products');
