<?php 

function ang_add_options_pages() {
	
	if( function_exists('acf_add_options_sub_page') ) {
    	acf_add_options_sub_page(array(
        	'title' 		=> 'General Options',
        	'parent' 		=> 'index.php',
        	'capability' 	=> 'edit_pages',
        	'slug'			=> 'general-options'
    	));
	}
}
add_action('init', 'ang_add_options_pages');

function ang_add_options() {
    if(function_exists("register_field_group")) {
        register_field_group(array (
            'id' => 'acf_general-options',
            'title' => 'General Options',
            'fields' => array (
                array (
                    'key' => 'field_537b3eal729f1',
                    'label' => 'Company Name',
                    'name' => 'company-name',
                    'type' => 'text',
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'formatting' => 'none',
                    'maxlength' => '',
                ),
                array (
                    'key' => 'field_537b8eal729f1',
                    'label' => 'Company Number',
                    'name' => 'company-number',
                    'type' => 'text',
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'formatting' => 'none',
                    'maxlength' => '',
                ),                
                array (
                    'key' => 'field_537b3ea4729f0',
                    'label' => 'Address',
                    'name' => 'address',
                    'type' => 'textarea',
                    'default_value' => '',
                    'placeholder' => '',
                    'maxlength' => '',
                    'rows' => '',
                    'formatting' => 'br',
                ),
                array (
                    'key' => 'field_537b3ead729f1',
                    'label' => 'Phone',
                    'name' => 'phone',
                    'type' => 'text',
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'formatting' => 'none',
                    'maxlength' => '',
                ),
                array (
                    'key' => 'field_537b3ede966be',
                    'label' => 'E-Mail',
                    'name' => 'e-mail',
                    'type' => 'email',
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'options_page',
                        'operator' => '==',
                        'value' => 'general-options',
                        'order_no' => 0,
                        'group_no' => 0,
                    ),
                ),
            ),
            'options' => array (
                'position' => 'normal',
                'layout' => 'default',
                'hide_on_screen' => array (
                ),
            ),
            'menu_order' => 0,
        ));
    }
}

add_action('init', 'ang_add_options');

function hide_acf_options_menu(){
    // remove_menu_page('acf-options');
}
add_action( 'admin_init', 'hide_acf_options_menu', 99 );

?>