<?php

/* Register Testimonials Post Type */
function ang_register_testimonials() {
	
	$labels = array(
    	'name' => 'Testimonials',
    	'singular_name' => 'Testimonial',
    	'add_new' => 'Add New',
    	'add_new_item' => 'Add New Testimonial',
    	'edit_item' => 'Edit Testimonial',
    	'new_item' => 'New Testimonial',
    	'all_items' => 'All Testimonials',
    	'view_item' => 'View Testimonials',
    	'search_items' => 'Search Testimonials',
    	'not_found' =>  'No Testimonials found',
    	'not_found_in_trash' => 'No Testimonial found in Trash', 
    	'parent_item_colon' => '',
    	'menu_name' => 'Testimonials'
  	);

  	$args = array(
    	'labels' => $labels,
    	'public' => true,
    	'publicly_queryable' => true,
    	'show_ui' => true,
        'show_in_menu' => true,
    	'query_var' => true,
    	'capability_type' => 'post',
    	'has_archive' => false, 
    	'hierarchical' => true,
    	'menu_position' => 2,
    	'menu_icon' => 'dashicons-star-filled',
    	'supports' => array('title')
  	); 

  	register_post_type( 'testimonial', $args );
  
}
add_action('init', 'ang_register_testimonials');

function ang_register_testimonial_meta() {
    if(function_exists("register_field_group"))
    {
        register_field_group(array (
            'id' => 'acf_testimonials',
            'title' => 'Testimonials',
            'fields' => array (
                array (
                    'key' => 'field_537b40756fe26',
                    'label' => 'Image',
                    'name' => 'image',
                    'type' => 'image',
                    'save_format' => 'url',
                    'preview_size' => 'thumbnail',
                    'library' => 'all',
                ),
                array (
                    'key' => 'field_537b407f6fe27',
                    'label' => 'Quote',
                    'name' => 'quote',
                    'type' => 'textarea',
                    'default_value' => '',
                    'placeholder' => '',
                    'maxlength' => '',
                    'rows' => 5,
                    'formatting' => 'none',
                ),
                array (
                    'key' => 'field_537b40896fe28',
                    'label' => 'Name',
                    'name' => 'name',
                    'type' => 'text',
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'formatting' => 'none',
                    'maxlength' => '',
                ),
                array (
                    'key' => 'field_537b408e6fe29',
                    'label' => 'Company/Position',
                    'name' => 'company',
                    'type' => 'text',
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'formatting' => 'none',
                    'maxlength' => '',
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'testimonial',
                        'order_no' => 0,
                        'group_no' => 0,
                    ),
                ),
            ),
            'options' => array (
                'position' => 'normal',
                'layout' => 'default',
                'hide_on_screen' => array (
                ),
            ),
            'menu_order' => 0,
        ));
    }
}
add_action('init', 'ang_register_testimonial_meta');