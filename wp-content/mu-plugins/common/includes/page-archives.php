<?php

// Add a submenu item labelled 'Description' to each public post type (filterable)
function ptd_add_submenu_page() {

    $post_types = ptd_get_post_types();

    foreach ($post_types as $post_type => $post_type_obj) {

        $parent_slug = 'edit.php?post_type='.$post_type;
        $page_title  = $post_type_obj->labels->name . ' description';
        $menu_title  = 'Link to Page';
        $capability  = $post_type_obj->cap->edit_posts;
        $menu_slug   = $post_type_obj->name . '-description';
        $function    = 'ptd_manage_description';

        add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function );

    }
}
add_action('admin_menu', 'ptd_add_submenu_page');

// Admin page to edit the description
function ptd_manage_description() {

    if ( empty( $_GET['post_type'] ) )
        return;

    $post_type = get_post_type_object( $_GET['post_type'] ); 
    
    $current_link = stripslashes(get_option($post_type->name . '-link')); 

    ?>
    <h2><?php echo esc_html( $post_type->labels->name ); ?> Linked Page</h2>

    <?php if ( isset( $_GET['updated'] ) && $_GET['updated'] ) { ?>

        <div id="message" class="updated">
            <p>Linked Page Updated.</p>
        </div>

    <?php } ?>

    <br/>
    <form method="POST">
        <select name="linked-page">
            <?php 
            $pages = get_all_pages(); 
            while($pages->have_posts()) : $pages->the_post(); ?>
                <option value="<?php the_ID(); ?>" <?php selected( $current_link, get_the_ID() ); ?>><?php the_title(); ?></option>
            <?php endwhile; wp_reset_postdata(); ?>
        </select>

        <input type="hidden" name="post_type" value="<?php echo esc_attr( $post_type->name ); ?>" />
        
        <p class="submit">
            <input class="button-primary" type="submit" name="ptd_update_linked_page" value="Update Linked Page"/>
        </p>

    </form>


<?php }

// Update the description
function ptd_update_description() {

    if(isset($_POST['ptd_update_linked_page'])) {

        $post_type = $_POST['post_type'];

        $linked_page = $_POST['linked-page'];

        update_option($post_type . '-link', $linked_page);

        wp_redirect( add_query_arg( array('page' => $post_type . '-description', 'updated' => 'true', 'post_type' => $post_type), $wp_get_referer ) ); exit;

    }

}
add_action('init', 'ptd_update_description');

// Front end function to display the description in the template
function linked_page() {

    $post_type = get_query_var( 'post_type' );
    $linked_page = stripslashes(get_option($post_type . '-link'));

    return $linked_page;

}

// Helper function to get all the post types (filterable)
function ptd_get_post_types() {

    $post_types = get_post_types(array(
        'public' => true,
        'has_archive' => true,
    ), 'objects');

    // Allow post types to be filterable
    $post_types = apply_filters('ptd_enabled_post_types', $post_types);

    return $post_types;

}

// Helper function to get an array of post types
function pta_get_enabled_post_type_array() {

    $post_types_array = array();

    $post_types = ptd_get_post_types();

    foreach ($post_types as $post_type => $post_type_obj) {

        $post_types_array[] = $post_type;

    }

    return $post_types_array;

}

function get_all_pages() {

    $args = array(
      'post_status' => 'publish',
      'post_type'   => 'page',
      'showposts'   => -1
    );
   
    $pages = new WP_Query($args);

    return $pages;
}

// Add Edit Description to the admin bar for better UX :-)
function ptd_admin_bar( $wp_admin_bar ) {

    $post_types = pta_get_enabled_post_type_array();

    if (is_post_type_archive( $post_types )) {

        $post_type = get_query_var( 'post_type' );

        $linked_page = get_option($post_type . '-link');

        $args = array(
            'id'    => 'edit_description',
            'title' => 'Edit Linked Page',
            'href'  => admin_url() . 'post.php?post=' . $linked_page . '&action=edit'
        );
    
        $wp_admin_bar->add_node( $args );
    }
}
add_action( 'admin_bar_menu', 'ptd_admin_bar', 999 );