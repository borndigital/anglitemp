<?php

function remove_dashboard_widgets() {
	global $wp_meta_boxes;
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );

function remove_extra_meta_boxes() {
	remove_meta_box( 'postcustom' , 'post' , 'normal' ); // custom fields for posts
	remove_meta_box( 'revisionsdiv' , 'post' , 'normal' ); // custom fields for posts	
	remove_meta_box( 'postcustom' , 'page' , 'normal' ); // custom fields for pages
	remove_meta_box( 'commentsdiv' , 'page' , 'normal' ); // recent comments for pages	
	remove_meta_box( 'tagsdiv-post_tag' , 'post' , 'side' ); // post tags
	remove_meta_box( 'tagsdiv-post_tag' , 'page' , 'side' ); // page tags
	remove_meta_box( 'trackbacksdiv' , 'post' , 'normal' ); // post trackbacks
	remove_meta_box( 'trackbacksdiv' , 'page' , 'normal' ); // page trackbacks	
	remove_meta_box(' slugdiv','post','normal'); // post slug
	remove_meta_box(' slugdiv','page','normal'); // page slug

}
add_action( 'admin_menu' , 'remove_extra_meta_boxes' );

function remove_menu_pages() {
	remove_menu_page('edit-comments.php');
}
add_action( 'admin_menu', 'remove_menu_pages' );

// Reorder Menu
function custom_menu_order($menu_ord) {
	if (!$menu_ord) return true;
       return array(
        'index.php', 
        'edit.php?post_type=page', 
        'edit.php?post_type=product', 
        'edit.php',
        'edit.php?post_type=project',
        'edit.php?post_type=testimonial',
        'nav-menus.php', 
    );
}
add_filter('custom_menu_order', 'custom_menu_order');
add_filter('menu_order', 'custom_menu_order');

add_filter( 'wpseo_use_page_analysis', '__return_false' );

add_action('admin_head', 'seo_fix', 10);
function seo_fix() { ?>

	<script type="text/javascript">
	
	(function($) {
		$(document).ready(function(){
			$('#wpseo_meta').insertAfter('#normal-sortables');
		});
	})(jQuery);
	</script>
	
<?php 
}

?>