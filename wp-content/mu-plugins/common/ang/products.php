<?php 
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function ang_product_meta( array $meta_boxes ) {
    
    $fields = array(
        array( 'id' => 'field-1',  'name' => 'Text input field', 'type' => 'text' ),
    );

    $meta_boxes[] = array(
        'title' => 'Product Info',
        'pages' => 'product',
        'fields' => $fields
    );

    return $meta_boxes;

}
add_filter( 'cmb_meta_boxes', 'ang_product_meta' );