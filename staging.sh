#!/bin/bash -x

echo ""
echo ""
tput bold
echo "### Updating websitesnewcastle..."
tput sgr0
ssh 'websitesnewcastle' bash -c "'
  cd ~/domains/bnt.websitesnewcastle.com/public_html/bnt
  git pull
'"
