#!/bin/bash -x

LIVE_DOMAIN='anglitemp.com'
LIVE_URL='http://bntinsulation.co.uk/wp-admin/'

echo ""
echo ""
tput bold
echo "### Updating live site..."
tput sgr0
ssh ${LIVE_DOMAIN} bash -c "'
  cd /var/www/bntinsulation
  git pull
'"
open ${LIVE_URL}
